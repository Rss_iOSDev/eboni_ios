////
////  PushNotificationManager.swift
////  Eboni
////
////  Created by Apple on 03/02/21.
////  Copyright © 2021 Apple. All rights reserved.
////
//
//import Foundation
//import Firebase
//import FirebaseFirestore
//import FirebaseMessaging
//import UIKit
//import UserNotifications
//class PushNotificationManager: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
//    let userID: String
//    init(userID: String) {
//        self.userID = userID
//        super.init()
//    }
//    func registerForPushNotifications() {
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//            // For iOS 10 data message (sent via FCM)
//            Messaging.messaging().delegate = self
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//        }
//        UIApplication.shared.registerForRemoteNotifications()
//        updateFirestorePushTokenIfNeeded()
//    }
//    func updateFirestorePushTokenIfNeeded() {
//        //        if let token = Messaging.messaging().fcmToken {
//        //            let usersRef = Firestore.firestore().collection("users_table").document(userID)
//        //            usersRef.setData(["fcmToken": token], merge: true)
//        //        }
//        if let token = Messaging.messaging().fcmToken {
//            userDef.setValue(token, forKey: "fcmToken")
//        }
//    }
//
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("Unable to register for remote notifications: \(error.localizedDescription)")
//    }
//    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
//    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
//    // the FCM registration token.
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        // With swizzling disabled you must set the APNs token here.
//        //  Messaging.messaging().apnsToken = deviceToken
//
//        print(deviceToken)
//    }
//
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print(remoteMessage.appData)
//    }
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        updateFirestorePushTokenIfNeeded()
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print(response)
//    }
//}
