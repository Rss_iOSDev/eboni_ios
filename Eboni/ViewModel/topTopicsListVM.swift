
import Foundation
import UIKit

class topTopicsListVM:BaseTableViewVM{
    
    var vc : followingViewController?
    var items:[Top10_topics] = []
    
    var viewCntollr : allChannelListViewController?
    
    var isType = ""
    
    var Filtereditems:[Top10_topics] = []
    
    var arrSelectTopic = [String]()
    
    init(controller: UIViewController?, items:[Top10_topics], selectId:[String]) {
        super.init(controller: controller)
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        self.Filtereditems = items
        self.arrSelectTopic = selectId
        self.identifierItem = "TopTopicsCell"
        self.nibItem = "TopTopicsCell"
        
    }
    
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TopTopicsCell", for: indexPath)  as? TopTopicsCell else { return UITableViewCell() }
        let obj = self.Filtereditems[indexPath.row]
        cell.setCell(item: obj)
        
        let catId = obj.cat_id
        if(self.arrSelectTopic.contains(catId!)){
            cell.btnSelect.isSelected = true
        }
        else{
            cell.btnSelect.isSelected = false
        }
        
        
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.clickOnSelect(_:)), for: .touchUpInside)
        return cell
    }
    
    override func getNumbersOfRows(in section: Int) -> Int {
        return Filtereditems.count
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        
    }
    
    override func willDisplayCell(_ indexPath: IndexPath, tableView: UITableView) {
        
        if(self.isType == "all"){
            
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    if indexPath.row == self.Filtereditems.count-1{
                        if(viewCntollr!.fetchingData == false){
                            viewCntollr!.fetchingData = true
                            viewCntollr!.spinner.isHidden = false
                            viewCntollr!.viewFooter.isHidden = false
                            viewCntollr!.hgtFooter.constant = 45
                            self.perform(#selector(loadData), with: nil, afterDelay: 2)
                        }
                        /*else{
                         viewContrlr!.viewFooter.isHidden = true
                         viewContrlr!.hgtFooter.constant = 0
                         viewContrlr!.spinner.isHidden = true
                         }*/
                    }
                }
            }
        }
        
        
    }
    
    @objc func loadData(){
        
        viewCntollr!.scrollIndex += 1
        viewCntollr!.callListOfTopTen(strCkeck: "load")
    }

    
    @objc func clickOnSelect(_ sender: UIButton)
    {
        let obj = self.Filtereditems[sender.tag]
        let catId = obj.cat_id
        let userId = (userGlobal?.id)!
        
//        let buttonPosition = sender.convert(CGPoint.zero, to: vc?.tblFollowingList)
//        let indexPath = vc?.tblFollowingList.indexPathForRow(at:buttonPosition)
//        let cell = vc?.tblFollowingList.cellForRow(at: indexPath!) as! TopTopicsCell

        APIClient.addFollowingItems(userId: userId, cat_id: catId!, objectTyp: "category") { (response) in
            OperationQueue.main.addOperation {
                if(response.message == "Your selection has been removed successfully"){
                    for i in 0..<self.arrSelectTopic.count{
                        let val = self.arrSelectTopic[i]
                        if(val == catId){
                            self.arrSelectTopic.remove(at: i)
                            break
                        }
                    }
                    
                    if(self.isType == "top"){
                        self.vc?.tblFollowingList.reloadData()
                    }
                    else{
                        self.viewCntollr?.tblChnlFollowingList.reloadData()
                    }
                    
                }
                else{
                    self.arrSelectTopic.append(catId!)
                    
                    if(self.isType == "top"){
                        self.vc?.tblFollowingList.reloadData()
                    }
                    else{
                        self.viewCntollr?.tblChnlFollowingList.reloadData()
                    }

                }
            }
        }
    }
}
