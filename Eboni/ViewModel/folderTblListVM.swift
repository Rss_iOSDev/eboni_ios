//
//  folderTblListVM.swift
//  Eboni
//
//  Created by Apple on 05/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation



import UIKit

class folderTblListVM: BaseTableViewVM {
    
    var vc : createFolderViewController?
    
    var items:[FolderList] = []
    
    var Filtereditems:[FolderList] = []
    
    var arrFolderid = NSMutableArray()
    var tblFolder: UITableView?
    var btnSave: UIButton?
    
    
    
    
    
    init(controller: UIViewController?, items:[FolderList]) {
        super.init(controller: controller)
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        self.Filtereditems = items
        self.identifierItem = "displayFolderCell"
        self.nibItem = "displayFolderCell"
        
    }
    
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "displayFolderCell", for: indexPath)  as? displayFolderCell else { return UITableViewCell() }
        
        let obj = self.Filtereditems[indexPath.row]
        cell.setCell(item: obj)
        return cell
        
    }
    
    override func getNumbersOfRows(in section: Int) -> Int {
        return Filtereditems.count
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        return 90
    }
    
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        
        let obj = self.Filtereditems[indexPath.row]
        
        if(obj.is_select == "true"){
            self.arrFolderid.removeAllObjects()
            // self.arrFolderid.add(obj.id!)
            var rr = self.Filtereditems.filter{ $0.id == obj.id! }.first
            rr?.is_select = "false"
            self.Filtereditems.remove(at: indexPath.row)
            self.Filtereditems.insert(rr!, at: indexPath.row)
            self.tblFolder?.reloadData()
            return
        }
        
        for i in 0..<Filtereditems.count{
            let objA = Filtereditems[i]
            var rr = self.Filtereditems.filter{ $0.id == objA.id! }.first
            rr?.is_select = "false"
            self.Filtereditems.remove(at: i)
            self.Filtereditems.insert(rr!, at: i)
        }
        
        
        self.arrFolderid.removeAllObjects()
        self.arrFolderid.add(obj.id!)
        var rr = self.Filtereditems.filter{ $0.id == obj.id! }.first
        rr?.is_select = "true"
        self.Filtereditems.remove(at: indexPath.row)
        self.Filtereditems.insert(rr!, at: indexPath.row)
        
        
        self.btnSave?.tag = indexPath.row
        self.btnSave?.addTarget(self, action: #selector(clickOnSaveBtn(_:)), for: .touchUpInside)
        
        self.tblFolder?.reloadData()
        
    }
    
    override func willDisplayCell(_ indexPath: IndexPath, tableView: UITableView) {
        
    }
    
    @objc func clickOnSaveBtn(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: vc!.view)
        
        let obj = self.Filtereditems[sender.tag]
        
        let userId = obj.userid
        let folderId = obj.id
        
        APIClient.addArticletoFolder(userId: userId!, folderId: folderId!, articleId: vc!.articleId) { (response) in
            if let dict = response as? NSDictionary {
                DispatchQueue.main.async {
                    APIClient.hideLoaderView(view: self.vc!.view)
                    let statsu = dict["status"] as! String
                    let messaage = dict["messaage"] as! String
                    if(statsu == "0"){
                        APIClient.showAlertMessage(vc: self.vc!, titleStr: "Hayti", messageStr: messaage)
                    }
                    else{
                        //.dismiss(animated: true, completion: nil)
                        if(self.vc?.openFrom == "share"){
                            self.vc?.dismiss(animated: true) {
                                self.vc?.viewShare.viewWillAppear(true)
                            }
                        }
                        else{
                            self.vc?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}
