
import UIKit

class FolderArticleCell: UITableViewCell {
    
    @IBOutlet weak var imgNews: UIImageView!
    
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblCatName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgPublisher: UIImageView!
    
    @IBOutlet weak var btnFolder: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    
    @IBOutlet weak var lblFiledValue: UILabel!
    @IBOutlet weak var btnPublsih: UIButton!
    
    @IBOutlet weak var imgFolder: UIImageView!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLikes: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!


    
    func setCell(item:NSDictionary){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgFolder.image = UIImage(named: "addFolderIcon")
            }
            else if(mode == "dark"){
                self.imgFolder.image = UIImage(named: "addFolderIconDark")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        print("Dark mode")
                        self.imgFolder.image = UIImage(named: "addFolderIconDark")
                    }
                    else {
                        self.imgFolder.image = UIImage(named: "addFolderIcon")
                    }
                }
                
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    print("Dark mode")
                    self.imgFolder.image = UIImage(named: "addFolderIconDark")
                }
                else {
                    self.imgFolder.image = UIImage(named: "addFolderIcon")
                }
            }
        }
        
        let img_url = item["field_image_url_value"] as? String
        self.imgNews.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))
        
        self.lblTitle.text = item["title"] as? String
        self.lblCreate.text = item["created"] as? String
        self.lblCatName.text = item["category_name"] as? String
        self.lblFiledValue.text = item["field_web_name_value"] as? String
        
        let publis_url = item["publisher_logo"] as? String
        self.imgPublisher.sd_setImage(with: URL.init(string: publis_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))

    }
}
