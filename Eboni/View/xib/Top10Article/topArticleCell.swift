//
//  topArticleCell.swift
//  Eboni
//
//  Created by Apple on 07/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class topArticleCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBookmark: UIView!
    
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblPageCount: UILabel!
    
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFiledValue: UILabel!
    
    @IBOutlet weak var imgPublisher: UIImageView!
    @IBOutlet weak var lblBottom: UILabel!
    
    @IBOutlet weak var btnFolder: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPublish: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLikes: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var imgFolder: UIImageView!
    
    
    
    
    
}
