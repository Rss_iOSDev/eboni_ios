

import UIKit

class folderListCell: UICollectionViewCell {
    
    @IBOutlet weak var imgFolder: UIImageView!
    @IBOutlet weak var lblFoldernam: UILabel!
    @IBOutlet weak var lblArticleCount: UILabel!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnBlur: UIButton!
    
    func setCell(item:allFolderList){
        let img_url = item.folder_thumb
        self.imgFolder.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: ""))
        self.lblFoldernam.text = item.folder_name
        self.lblArticleCount.text = "\(item.articleCount!) saved"
    }

}
