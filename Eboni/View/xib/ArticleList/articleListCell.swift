//
//  articleListCell.swift
//  Eboni
//
//  Created by Apple on 07/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleMobileAds

class articleListCell: UITableViewCell {
    
    @IBOutlet weak var imgNews: UIImageView!
    
    @IBOutlet weak var lblCreate: UILabel!
    @IBOutlet weak var lblCatName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgPublisher: UIImageView!
    
    @IBOutlet weak var btnFolder: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var lblFiledValue: UILabel!
    @IBOutlet weak var btnPublsih: UIButton!
    
    @IBOutlet weak var btnSeeAll: UIButton!
    
    @IBOutlet weak var imgFolder: UIImageView!
    
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLikes: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!

    
    var adLoader: GADAdLoader!
    var nativeAdView: GADUnifiedNativeAdView!
    var heightConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var viewBannerCell: GADUnifiedNativeAdView!
    @IBOutlet weak var stackBanner: UIStackView!
    @IBOutlet weak var bannerHgt: NSLayoutConstraint!
    
    func setCell(item:NSDictionary){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgFolder.image = UIImage(named: "addFolderIcon")
            }
            else if(mode == "dark"){
                self.imgFolder.image = UIImage(named: "addFolderIconDark")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        print("Dark mode")
                        self.imgFolder.image = UIImage(named: "addFolderIconDark")
                    }
                    else {
                        self.imgFolder.image = UIImage(named: "addFolderIcon")
                    }
                }
                
            }
        }
        else{
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        print("Dark mode")
                        self.imgFolder.image = UIImage(named: "addFolderIconDark")
                    }
                    else {
                        self.imgFolder.image = UIImage(named: "addFolderIcon")
                    }
                }
        }
        
        
        let img_url = item["field_image_url_value"] as? String
//        let ss = self.sizeOfImageAt(url: URL.init(string: img_url!)!)
//
//        if (ss != nil){
            self.imgNews.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: ""))
//            self.imgHgt.constant = ss!.height
//        }
//        else{
//            self.imgNews.backgroundColor = .systemGroupedBackground
//            self.imgHgt.constant = 283
//        }
        
        // self.imgNews.clipsToBounds = true
        // self.imgNews.layer.cornerRadius = 8
        // self.imgNews.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        self.lblTitle.text = item["title"] as? String
        self.lblCreate.text = item["created"] as? String
        self.lblCatName.text = item["category_name"] as? String
        self.lblFiledValue.text = item["field_web_name_value"] as? String
        
        let publis_url = item["publisher_logo"] as? String
        self.imgPublisher.sd_setImage(with: URL.init(string: publis_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))
        
    }
    
    func sizeOfImageAt(url: URL) -> CGSize? {
        // with CGImageSource we avoid loading the whole image into memory
        guard let source = CGImageSourceCreateWithURL(url as CFURL, nil) else {
            return nil
        }
        
        let propertiesOptions = [kCGImageSourceShouldCache: false] as CFDictionary
        guard let properties = CGImageSourceCopyPropertiesAtIndex(source, 0, propertiesOptions) as? [CFString: Any] else {
            return nil
        }
        
        if let width = properties[kCGImagePropertyPixelWidth] as? CGFloat,
           let height = properties[kCGImagePropertyPixelHeight] as? CGFloat {
            return CGSize(width: width, height: height)
        } else {
            return nil
        }
    }
    
}





