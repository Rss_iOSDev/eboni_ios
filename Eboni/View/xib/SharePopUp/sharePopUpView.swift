



import UIKit
protocol sharePopupDelegate{
    
    func clickOnBack()
    func clickOnActionBtn(strtyp: String)
}
class sharePopUpView: UIView {
    let nibName = "sharePopUpView"
    var contentView: UIView?
    var shareDelegate: sharePopupDelegate!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        
        contentView = view
        view.frame = self.bounds
        
        
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        shareDelegate.clickOnBack()

    }
    
    @IBAction func btnAdd(_ sender: UIButton) {
        shareDelegate.clickOnActionBtn(strtyp: "add")

    }
    
    @IBAction func btnShare(_ sender: UIButton) {
        shareDelegate.clickOnActionBtn(strtyp: "share")

    }
    
    @IBAction func btnCopy(_ sender: UIButton) {
        shareDelegate.clickOnActionBtn(strtyp: "copy")

    }



}
