
import UIKit

protocol createfolderDelegate{
    func clickOnBack()
}
class CreateFolderPopUp: UIView {
    
    let nibName = "CreateFolderPopUp"
    var contentView: UIView?
    var createfolderDelegate: createfolderDelegate!
    
    @IBOutlet weak var txtfolderName : UITextField!
    @IBOutlet weak var btnCreate : UIButton!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        
        contentView = view
        view.frame = self.bounds
        
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        createfolderDelegate.clickOnBack()
    }
    

}
