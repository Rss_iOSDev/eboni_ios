

import UIKit
protocol removefolderDelegate{
    func clickOnBack()
}
class RemoveItemfromFolder: UIView {
    
    let nibName = "RemoveItemfromFolder"
    var contentView: UIView?
    var removefolderDelegate: removefolderDelegate!
    
    @IBOutlet weak var lblFolderTitle : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        
        contentView = view
        view.frame = self.bounds
        
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        removefolderDelegate.clickOnBack()
    }
    

}
