
import UIKit

class displayFolderCell: UITableViewCell {
    
    @IBOutlet weak var imgFolder: UIImageView!
    @IBOutlet weak var lblFolerNam: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    
    func setCell(item:FolderList){
        let img_url = item.folder_thumb
        self.imgFolder.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: ""))
        
        self.lblFolerNam.text = item.folder_name
        
        if(item.is_select! == "false"){
            self.imgSelect.isHidden = true
        }
        else{
            self.imgSelect.isHidden = false
        }
    }

}
