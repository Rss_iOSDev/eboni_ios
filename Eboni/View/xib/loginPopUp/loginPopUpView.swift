//
//  loginPopUpView.swift
//  Eboni
//
//  Created by Apple on 21/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//


import UIKit
protocol loginPopupDelegate{
    
    func clickOnBack()
    func clickOnLoginBtn(strTyp: String)
}

class loginPopUpView: UIView {
    let nibName = "loginPopUpView"
    var contentView: UIView?
    var loginPopupViewDelegate: loginPopupDelegate!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])

        contentView = view
        view.frame = self.bounds
        
    }
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        userDef.set("true", forKey: "isBack")
        loginPopupViewDelegate.clickOnBack()

    }
    
    @IBAction func btnGoogleLogin(_ sender: UIButton) {
        loginPopupViewDelegate.clickOnLoginBtn(strTyp: "gmail")

    }
    
    @IBAction func btnAppleLogin(_ sender: UIButton) {
        loginPopupViewDelegate.clickOnLoginBtn(strTyp: "apple")

    }
    
    @IBAction func btnFbLogin(_ sender: UIButton) {
        loginPopupViewDelegate.clickOnLoginBtn(strTyp: "facebook")

    }

    
    @IBAction func clickOnSignupOrLogin(_ sender: UIButton)
    {
        loginPopupViewDelegate.clickOnLoginBtn(strTyp: "email")

    }



}
