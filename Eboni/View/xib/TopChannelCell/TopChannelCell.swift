//
//  TopChannelCell.swift
//  Eboni
//
//  Created by Apple on 21/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


import UIKit

class TopChannelCell: UITableViewCell {
    
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    
    func setCell(item:Top10_channels){
        
        self.lblName.text = item.wEB_NAME
        let img_url = item.wEB_IMAGE_LOGO
        self.imgCat.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))
    }

}
