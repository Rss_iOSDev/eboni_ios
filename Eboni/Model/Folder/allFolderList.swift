

import Foundation
struct allFolderList : Codable {
    let id : String?
    let userid : String?
    let folder_name : String?
    let folder_thumb : String?
    let articleCount : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case userid = "userid"
        case folder_name = "folder_name"
        case folder_thumb = "folder_thumb"
        case articleCount = "articleCount"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        folder_name = try values.decodeIfPresent(String.self, forKey: .folder_name)
        folder_thumb = try values.decodeIfPresent(String.self, forKey: .folder_thumb)
        articleCount = try values.decodeIfPresent(String.self, forKey: .articleCount)
    }

}
