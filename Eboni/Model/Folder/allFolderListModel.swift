

import Foundation
struct allFolderListModel : Codable {
	let total_no_of_page : Int?
	let folderList : [allFolderList]?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case total_no_of_page = "total_no_of_page"
		case folderList = "folderList"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		total_no_of_page = try values.decodeIfPresent(Int.self, forKey: .total_no_of_page)
		folderList = try values.decodeIfPresent([allFolderList].self, forKey: .folderList)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
