import Foundation
struct FolderList : Codable {
	let id : String?
	let userid : String?
	let folder_name : String?
	let folder_thumb : String?
    var is_select : String?

	enum CodingKeys: String, CodingKey {
		case id = "id"
		case userid = "userid"
		case folder_name = "folder_name"
		case folder_thumb = "folder_thumb"
        case is_select = "is_select"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		userid = try values.decodeIfPresent(String.self, forKey: .userid)
		folder_name = try values.decodeIfPresent(String.self, forKey: .folder_name)
		folder_thumb = try values.decodeIfPresent(String.self, forKey: .folder_thumb)
        is_select = try values.decodeIfPresent(String.self, forKey: .is_select)
	}
}
