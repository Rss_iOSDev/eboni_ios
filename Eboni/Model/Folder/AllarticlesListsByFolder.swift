

import Foundation
struct AllarticlesListsByFolder : Codable {
	let article_id : String?
	let folder_id : String?
	let userid : String?
	let title : String?
	let created : String?
	let folder_name : String?
	let field_image_url_value : String?
	let full_url : String?
	let field_web_name_value : String?
	let publisher_id : String?
	let publisher_logo : String?
	let category_id : String?
	let category_name : String?
	let random_field : String?
    var isFavorite : String?
    var total_likes : String?


	enum CodingKeys: String, CodingKey {

		case article_id = "article_id"
		case folder_id = "folder_id"
		case userid = "userid"
		case title = "title"
		case created = "created"
		case folder_name = "folder_name"
		case field_image_url_value = "field_image_url_value"
		case full_url = "full_url"
		case field_web_name_value = "field_web_name_value"
		case publisher_id = "publisher_id"
		case publisher_logo = "publisher_logo"
		case category_id = "category_id"
		case category_name = "category_name"
		case random_field = "random_field"
        case isFavorite = "isFavorite"
        case total_likes = "total_likes"

	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		article_id = try values.decodeIfPresent(String.self, forKey: .article_id)
		folder_id = try values.decodeIfPresent(String.self, forKey: .folder_id)
		userid = try values.decodeIfPresent(String.self, forKey: .userid)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		folder_name = try values.decodeIfPresent(String.self, forKey: .folder_name)
		field_image_url_value = try values.decodeIfPresent(String.self, forKey: .field_image_url_value)
		full_url = try values.decodeIfPresent(String.self, forKey: .full_url)
		field_web_name_value = try values.decodeIfPresent(String.self, forKey: .field_web_name_value)
		publisher_id = try values.decodeIfPresent(String.self, forKey: .publisher_id)
		publisher_logo = try values.decodeIfPresent(String.self, forKey: .publisher_logo)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
		random_field = try values.decodeIfPresent(String.self, forKey: .random_field)
        isFavorite = try values.decodeIfPresent(String.self, forKey: .isFavorite)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)

	}

}
