

import Foundation
struct User_info : Codable {
	let status : Int?
	let id : String?
	let email : String?
	let first_name : String?
	let last_name : String?
	let city : String?
    let country : String?
    let postal_code : String?
    let profile_thumb_url : String?
    let tokans : String?
    let registration_date : String?
	let message : String?


    enum CodingKeys: String, CodingKey {
        case status = "status"
		case id = "id"
		case email = "email"
		case first_name = "first_name"
		case last_name = "last_name"
		case city = "city"
        case country = "country"
        case postal_code = "postal_code"
        case profile_thumb_url = "profile_thumb_url"
        case tokans = "tokans"
        case registration_date = "registration_date"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        postal_code = try values.decodeIfPresent(String.self, forKey: .postal_code)
        profile_thumb_url = try values.decodeIfPresent(String.self, forKey: .profile_thumb_url)
        tokans = try values.decodeIfPresent(String.self, forKey: .tokans)
        registration_date = try values.decodeIfPresent(String.self, forKey: .registration_date)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}

