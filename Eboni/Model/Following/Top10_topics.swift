

import Foundation
struct Top10_topics : Codable {
	let cat_name : String?
	let cat_id : String?

	enum CodingKeys: String, CodingKey {

		case cat_name = "cat_name"
		case cat_id = "cat_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		cat_name = try values.decodeIfPresent(String.self, forKey: .cat_name)
		cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
	}

}


struct allCategoryFollowingModel : Codable {
    let total_no_of_page : Int?
    let category_info : [Top10_topics]?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case total_no_of_page = "total_no_of_page"
        case category_info = "category_info"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_no_of_page = try values.decodeIfPresent(Int.self, forKey: .total_no_of_page)
        category_info = try values.decodeIfPresent([Top10_topics].self, forKey: .category_info)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}


struct selctFollowingModel : Codable {
    let topics : [String]?
    let channels : [String]?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case topics = "topics"
        case channels = "channels"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        topics = try values.decodeIfPresent([String].self, forKey: .topics)
        channels = try values.decodeIfPresent([String].self, forKey: .channels)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}

struct allChannelList : Codable {
    let allwebnames : [Top10_channels]?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case allwebnames = "allwebnames"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        allwebnames = try values.decodeIfPresent([Top10_channels].self, forKey: .allwebnames)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}
