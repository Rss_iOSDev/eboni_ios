

import Foundation
struct Top10_channels : Codable {
	let iD : String?
	let wEB_NAME : String?
	let wEB_IMAGE_LOGO : String?

	enum CodingKeys: String, CodingKey {

		case iD = "ID"
		case wEB_NAME = "WEB_NAME"
		case wEB_IMAGE_LOGO = "WEB_IMAGE_LOGO"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		iD = try values.decodeIfPresent(String.self, forKey: .iD)
		wEB_NAME = try values.decodeIfPresent(String.self, forKey: .wEB_NAME)
		wEB_IMAGE_LOGO = try values.decodeIfPresent(String.self, forKey: .wEB_IMAGE_LOGO)
	}

}




