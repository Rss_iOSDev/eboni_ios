
import Foundation
struct loginModel : Codable {
	let user_info : User_info?
	let method : String?

	enum CodingKeys: String, CodingKey {
		case user_info = "user_info"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_info = try values.decodeIfPresent(User_info.self, forKey: .user_info)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}
}
//https://www.eboniapp.com/webapi-channellist-search?keyword=&page=4&userid=58
