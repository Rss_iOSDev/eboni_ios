import Foundation

struct blackModel : Codable {
	let blackHistory_info : BlackHistory_info?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case blackHistory_info = "blackHistory_info"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		blackHistory_info = try values.decodeIfPresent(BlackHistory_info.self, forKey: .blackHistory_info)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
