

import Foundation
struct belowModel : Codable {
	let total_no_of_page : Int?
	let allarticlesLists : [AllarticlesLists_below]?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case total_no_of_page = "total_no_of_page"
		case allarticlesLists = "allarticlesLists"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		total_no_of_page = try values.decodeIfPresent(Int.self, forKey: .total_no_of_page)
		allarticlesLists = try values.decodeIfPresent([AllarticlesLists_below].self, forKey: .allarticlesLists)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
