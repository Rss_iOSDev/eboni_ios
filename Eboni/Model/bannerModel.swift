

import Foundation

struct bannerModel : Codable {
	let homepagebox_message : Homepagebox_message?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case homepagebox_message = "homepagebox_message"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		homepagebox_message = try values.decodeIfPresent(Homepagebox_message.self, forKey: .homepagebox_message)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
