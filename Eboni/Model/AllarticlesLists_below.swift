

import Foundation
struct AllarticlesLists_below : Codable {
	let nid : String?
	let title : String?
	let created : String?
	let field_image_url_value : String?
	let full_url : String?
	let field_web_name_value : String?
	let publisher_id : String?
	let publisher_logo : String?
	let category_id : String?
	let category_name : String?
	let color_code : String?
    var isFavorite : String?
    var total_likes : String?
    


	enum CodingKeys: String, CodingKey {

		case nid = "nid"
		case title = "title"
		case created = "created"
		case field_image_url_value = "field_image_url_value"
		case full_url = "full_url"
		case field_web_name_value = "field_web_name_value"
		case publisher_id = "publisher_id"
		case publisher_logo = "publisher_logo"
		case category_id = "category_id"
		case category_name = "category_name"
		case color_code = "color_code"
        case isFavorite = "isFavorite"
        case total_likes = "total_likes"

	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		nid = try values.decodeIfPresent(String.self, forKey: .nid)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		field_image_url_value = try values.decodeIfPresent(String.self, forKey: .field_image_url_value)
		full_url = try values.decodeIfPresent(String.self, forKey: .full_url)
		field_web_name_value = try values.decodeIfPresent(String.self, forKey: .field_web_name_value)
		publisher_id = try values.decodeIfPresent(String.self, forKey: .publisher_id)
		publisher_logo = try values.decodeIfPresent(String.self, forKey: .publisher_logo)
		category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
		category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
		color_code = try values.decodeIfPresent(String.self, forKey: .color_code)
        isFavorite = try values.decodeIfPresent(String.self, forKey: .isFavorite)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)

	}

}
