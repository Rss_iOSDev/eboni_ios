
import Foundation
struct Homepagebox_message : Codable {
	let banner_image : String?
	let link_url : String?
    let ios_link_url : String?

	enum CodingKeys: String, CodingKey {
		case banner_image = "banner_image"
		case link_url = "link_url"
        case ios_link_url = "ios_link_url"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		banner_image = try values.decodeIfPresent(String.self, forKey: .banner_image)
		link_url = try values.decodeIfPresent(String.self, forKey: .link_url)
        ios_link_url = try values.decodeIfPresent(String.self, forKey: .ios_link_url)
	}

}
