
import Foundation

struct AllarticlesLists : Codable {
	let nid : String?
	let title : String?
	let created : String?
	let field_image_url_value : String?
	let full_url : String?
	let field_web_name_value : String?
	let publisher_id : String?
	let publisher_logo : String?
	let field_category_target_id : String?
	let field_no_of_view_value : String?
    var isFavorite : String?
    var total_likes : String?
    var totalfield_no_of_view_value : String?
    var nodealias : String?
    var big_image_url_value : String?
    
    
    
        
    
	enum CodingKeys: String, CodingKey {

		case nid = "nid"
		case title = "title"
		case created = "created"
		case field_image_url_value = "field_image_url_value"
		case full_url = "full_url"
		case field_web_name_value = "field_web_name_value"
		case publisher_id = "publisher_id"
		case publisher_logo = "publisher_logo"
		case field_category_target_id = "field_category_target_id"
		case field_no_of_view_value = "field_no_of_view_value"
        case isFavorite = "isFavorite"
        case total_likes = "total_likes"
        case totalfield_no_of_view_value = "totalfield_no_of_view_value"
        case nodealias = "nodealias"
        case big_image_url_value = "big_image_url_value"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		nid = try values.decodeIfPresent(String.self, forKey: .nid)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		created = try values.decodeIfPresent(String.self, forKey: .created)
		field_image_url_value = try values.decodeIfPresent(String.self, forKey: .field_image_url_value)
		full_url = try values.decodeIfPresent(String.self, forKey: .full_url)
		field_web_name_value = try values.decodeIfPresent(String.self, forKey: .field_web_name_value)
		publisher_id = try values.decodeIfPresent(String.self, forKey: .publisher_id)
		publisher_logo = try values.decodeIfPresent(String.self, forKey: .publisher_logo)
		field_category_target_id = try values.decodeIfPresent(String.self, forKey: .field_category_target_id)
		field_no_of_view_value = try values.decodeIfPresent(String.self, forKey: .field_no_of_view_value)
        isFavorite = try values.decodeIfPresent(String.self, forKey: .isFavorite)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        totalfield_no_of_view_value = try values.decodeIfPresent(String.self, forKey: .totalfield_no_of_view_value)
        nodealias = try values.decodeIfPresent(String.self, forKey: .nodealias)
        big_image_url_value = try values.decodeIfPresent(String.self, forKey: .big_image_url_value)
	}

}
