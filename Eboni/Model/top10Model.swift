

import Foundation
struct top10Model : Codable {
	let allarticlesLists : [AllarticlesLists]?
	let method : String?

	enum CodingKeys: String, CodingKey {
		case allarticlesLists = "allarticlesLists"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		allarticlesLists = try values.decodeIfPresent([AllarticlesLists].self, forKey: .allarticlesLists)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}
}
