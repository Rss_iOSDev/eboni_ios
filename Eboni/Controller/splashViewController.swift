//
//  splashViewController.swift
//  Eboni
//
//  Created by Apple on 12/03/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class splashViewController: UIViewController {
    
    @IBOutlet weak var imgLogo : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setModeofApp()
        
        self.perform(#selector(setRootView), with: nil, afterDelay: 1.0)
        

        // Do any additional setup after loading the view.
    }
    
     func setModeofApp(){
       
       if let mode = userDef.value(forKey: "isMode") as? String
       {
           if(mode == "light"){
               self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
           }
           else if(mode == "dark"){
              self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
           }
           else if(mode == "auto"){
               if #available(iOS 13.0, *) {
                   if UITraitCollection.current.userInterfaceStyle == .dark {
                       print("Dark mode")
                       self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                   }
                   else {
                      self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                   }
               }
           }
       }
       else{
           if #available(iOS 13.0, *) {
               if UITraitCollection.current.userInterfaceStyle == .dark {
                   print("Dark mode")
                   self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
               }
               else {
                   self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
               }
           }
       }
   }
    
    @objc func setRootView(){
        
        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
            let decoder = JSONDecoder()
            if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                userGlobal = userM
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(initialViewController, animated: true)
//                let rootViewController = initialViewController
//                let navigationController = UINavigationController()
//                navigationController.navigationBar.isHidden = true
//                navigationController.viewControllers = [rootViewController]
//                window?.rootViewController = navigationController
//                window?.makeKeyAndVisible()
            }
        }
        else{
            
//            let _window = UIWindow(frame: UIScreen.main.bounds)
//            window = _window
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            self.navigationController?.pushViewController(initialViewController, animated: true)
//            let rootViewController = initialViewController
//            let navigationController = UINavigationController()
//            navigationController.navigationBar.isHidden = true
//            navigationController.viewControllers = [rootViewController]
//            window?.rootViewController = navigationController
//            window?.makeKeyAndVisible()
        }

    }
}
