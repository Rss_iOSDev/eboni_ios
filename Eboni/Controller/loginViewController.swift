//
//  loginViewController.swift
//  Eboni
//
//  Created by Apple on 21/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import FBSDKLoginKit
import Firebase
import CoreLocation
import SwiftKeychainWrapper

class loginViewController: UIViewController {
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
   
    var locationManager: CLLocationManager!
    var loca_curr : CLLocation?
    
    var address = ""
    var city = ""
    var country = ""
    var postalcode = ""
    
    var isLoginScreenOpen = ""
    
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var imgBg : UIImageView!
    
    @IBOutlet weak var lblHeadLine: UILabel!
    
    @IBOutlet weak var viewAppleLogin: UIView!
    
    @IBOutlet weak var appleLoginBtn: UIButton!
    
   // @IBOutlet weak var appleLoginBtn: MyAuthorizationAppleIDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.lblHeadLine.text = "Find daily news from all your favorite \n Black publishers in one app."
        
        self.allowToGetLocation()
        
     //   self.setModeofApp()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Do any additional setup after loading the view.
        
        self.reFreshToken()
        self.createAppleLoginButton()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

    }
    
    func reFreshToken(){
        if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
            if let device_token = userDef.object(forKey: "device_token") as? String{
                APIClient.sendRefeshToken(device_id: device_token, refreshTokens: fcmToken) { (response) in
                    print(response)
                }
            }
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func googleLogin(_ sender : UIButton)
    {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func clickOnFbLogin(_ sender: UIButton)
    {
        facebookSignup()
    }
    
    @IBAction func clickOnSignupOrLogin(_ sender: UIButton)
    {
        let login = self.storyboard?.instantiateViewController(identifier: "loginByEmailVC") as! loginByEmailVC
        login.isViewOpenFrom = "page_1"
        self.navigationController?.pushViewController(login, animated: true)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
       // NotificationCenter.default.addObserver(self, selector: #selector(setModeofApp), name: UIApplication.willEnterForegroundNotification, object: nil)

    }
}

extension loginViewController :CLLocationManagerDelegate{
    
    func allowToGetLocation()
    {
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        self.loca_curr = location
        self.getAddressFromLatLong(expLoc: self.loca_curr!)
        
    }
    
    func getAddressFromLatLong(expLoc: CLLocation){
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(expLoc, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            
            if(placemarks != nil){
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    // Location name
                    if pm.name != nil {
                        self.address = pm.name!
                    }
                    
                    if pm.locality != nil{
                        self.city = pm.locality!
                    }
                    
                    // Street address
                    if pm.postalCode != nil {
                        self.postalcode = pm.postalCode!
                    }
                    
                    // Country
                    if pm.country != nil {
                        self.country =  pm.country!
                    }
              }
            }
            
//            let pm = placemarks! as [CLPlacemark]
//            
//            if pm.count > 0 {
//                let pm = placemarks![0]
//                
//                // Location name
//                if pm.name != nil {
//                    self.address = pm.name!
//                }
//                
//                if pm.locality != nil{
//                    self.city = pm.locality!
//                }
//                
//                // Street address
//                if pm.postalCode != nil {
//                    self.postalcode = pm.postalCode!
//                }
//                
//                // Country
//                if pm.country != nil {
//                    self.country =  pm.country!
//                }
//          }
        })
        
    }


}

extension loginViewController {
    
    @IBAction func clickOnSkipBtn(_ sender : UIButton)
    {
        let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
        self.navigationController?.pushViewController(home, animated: true)
    }
}

extension loginViewController : GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!)
    {
        if let _ = error {
            print("errrrr = \(error.localizedDescription)")
        } else {
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let  email = user.profile.email
            
            Analytics.logEvent("login_event", parameters: [
                "type": "google"
            ])
            
            var tokan = ""
            if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
                tokan = fcmToken
            }
            
            var deviceID = ""
            if let device_token = userDef.object(forKey: "device_token") as? String{
                deviceID = device_token
            }
            
            APIClient.callLoginapi(email: email!, firstName: givenName!, lastName: familyName!, address: self.address, city: city, country: country, postalcode: postalcode, profile: "", appname: Endpoints.Environment.appName, regsource: "google", tokens: tokan, device_id: deviceID) { (loginModel) in
                //save user model in userdefault and navigate to home
                
                OperationQueue.main.addOperation {
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(loginModel.user_info!) {
                        userDef.set(encoded, forKey: "userInfo")
                        userDef.set("google", forKey: "socela_media")
                    }
                    
                    if let userInfo = userDef.object(forKey: "userInfo") as? Data {
                        let decoder = JSONDecoder()
                        if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                            userGlobal = userM
                            let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                            self.navigationController?.pushViewController(home, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        // customLoader.hide()
        self.present(viewController, animated: false, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
}

extension loginViewController : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {

    func createAppleLoginButton(){
        
//        let authorizationButton = ASAuthorizationAppleIDButton()
//
//        authorizationButton.translatesAutoresizingMaskIntoConstraints = false
//
//        self.viewAppleLogin.addSubview(authorizationButton)
//
//        NSLayoutConstraint.activate([
//            authorizationButton.centerXAnchor.constraint(equalToSystemSpacingAfter: self.viewAppleLogin.centerXAnchor, multiplier: 1),
//            authorizationButton.centerYAnchor.constraint(equalToSystemSpacingBelow: self.viewAppleLogin.centerYAnchor, multiplier: 1),
//            authorizationButton.heightAnchor.constraint(equalToConstant: 65),
//            authorizationButton.widthAnchor.constraint(equalToConstant: self.viewAppleLogin.frame.width)
//        ])
//
        self.appleLoginBtn.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)

    }
    
    @objc private func handleLogInWithAppleIDButtonPress() {
        
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
    }
        
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }


    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)
    {
        //need to save details in firsttime
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let _ = appleIDCredential.user

            if let email = appleIDCredential.email {

                let fullName = appleIDCredential.fullName
                let Firstname = (fullName?.givenName)!
                let Lastname = (fullName?.familyName)!
                
                print("firts time")

                KeychainWrapper.standard.set(appleIDCredential, forKey: "appleCred")
                self.getCredentialsFomLoginAndCallApi(strFrstname: Firstname, strLastname: Lastname, strEmail: email)

            }

            else{
                
                if let appleCred = KeychainWrapper.standard.object(forKey: "appleCred") as? ASAuthorizationAppleIDCredential{
                    
                    let email = appleCred.email
                    let fullName = appleCred.fullName
                    let Firstname = (fullName?.givenName)!
                    let Lastname = (fullName?.familyName)!

                    self.getCredentialsFomLoginAndCallApi(strFrstname: Firstname, strLastname: Lastname, strEmail: email)
                }
                       
            }
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error)
    {
        // Handle error.
    }
    
    func getCredentialsFomLoginAndCallApi(strFrstname: String?, strLastname: String?, strEmail: String?){
        

        var tokan = ""
        if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
            tokan = fcmToken
            //APIClient.showAlertMessage(vc: self, titleStr: "", messageStr: tokan)
        }

        Analytics.logEvent("login_event", parameters: [
            "type": "apple"
        ])
        
        var deviceID = ""
        if let device_token = userDef.object(forKey: "device_token") as? String{
            deviceID = device_token
        }


        APIClient.callLoginapi(email: strEmail!, firstName: strFrstname!, lastName: strLastname!, address: address, city: self.city, country: self.country, postalcode: self.postalcode, profile: "", appname: Endpoints.Environment.appName, regsource: "apple", tokens: tokan, device_id: deviceID) { (loginModel) in

            //save user model in userdefault and navigate to home

            OperationQueue.main.addOperation {
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(loginModel.user_info!) {
                    userDef.set(encoded, forKey: "userInfo")
                    userDef.set("apple", forKey: "socela_media")
                }

                if let userInfo = userDef.object(forKey: "userInfo") as? Data {
                    let decoder = JSONDecoder()
                    if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                        userGlobal = userM
                        let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                        self.navigationController?.pushViewController(home, animated: true)
                    }
                }
            }
        }
    }
    
    


}
extension loginViewController {
    
    func facebookSignup()
    {
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["email"], from: self, handler: { (result, error) -> Void in
            
            if (error == nil) {
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.isCancelled) {
                    //Show Cancel alert
                } else if(fbloginresult.grantedPermissions.contains("email")) {
                    self.returnUserData()
                }
                else {
                    print("fbbbbb = \(fbloginresult)")
                }
            }
        })
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, name, picture.type(large), email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                // print("\n\n Error: \(String(describing: error))")
            } else {
                let resultDic = result as! NSDictionary
                print("\n\n  fetched user: \(String(describing: resultDic))")
                
                var fname = ""
                var lname = ""
                
                if let name = resultDic.value(forKey:"name")! as? String{
                    let fullNameArr = name.components(separatedBy: " ")
                    fname = fullNameArr[0]
                    lname = fullNameArr[1]
                }
                let email = resultDic.value(forKey:"email")! as! String
                
                var tokan = ""
                if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
                    tokan = fcmToken
                }
                
                Analytics.logEvent("login_event", parameters: [
                    "type": "facebook"
                ])
                
                var deviceID = ""
                if let device_token = userDef.object(forKey: "device_token") as? String{
                    deviceID = device_token
                }

                
                APIClient.callLoginapi(email: email, firstName: fname, lastName: lname, address: self.address, city: self.city, country: self.country, postalcode: self.postalcode, profile: "", appname: Endpoints.Environment.appName, regsource: "facebook", tokens: tokan, device_id: deviceID) { (loginModel) in
                    
                    //save user model in userdefault and navigate to home
                    
                    OperationQueue.main.addOperation {
                        let encoder = JSONEncoder()
                        if let encoded = try? encoder.encode(loginModel.user_info!) {
                            userDef.set(encoded, forKey: "userInfo")
                            userDef.set("facebook", forKey: "socela_media")
                        }
                        
                        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
                            let decoder = JSONDecoder()
                            if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                                userGlobal = userM
                                let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                                self.navigationController?.pushViewController(home, animated: true)
                            }
                        }
                    }
                }
            }
        })
    }
}


