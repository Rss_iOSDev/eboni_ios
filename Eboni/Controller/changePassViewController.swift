//
//  changePassViewController.swift
//  Eboni
//
//  Created by Apple on 26/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class changePassViewController: UIViewController {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConrfmPAss: UITextField!
    
    var passEmail = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)


        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
            
    }
    
}

extension changePassViewController {
    
    @IBAction func clickOnContinueBtn(_ sender: UIButton){
       if(txtPassword.text! == "" || txtPassword.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Password should not left blank.")
        }
        else if(txtConrfmPAss.text! == "" || txtConrfmPAss.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please enter password again.")
        }
        else if(txtPassword.text! != txtConrfmPAss.text){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Password do not match.")
        }
      
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            APIClient.showLoaderView(view: self.view)
            self.changePassApi()
        }
        else{
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please check Internet connection")
        }
    }
}


extension changePassViewController {
    
    func changePassApi()
    {

        APIClient.resetPassApi(userMail: self.passEmail, resetpassword: self.txtPassword.text!) { (response) in
            
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                print(response)
                
                if let resp = response as? NSDictionary{
                    
                    if let user_info = resp["user_info"] as? NSDictionary{
                        
                        if let msg = user_info["message"] as? String{
                            
                            let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                                for aViewController in viewControllers {
                                    if aViewController is loginByEmailVC {
                                        self.navigationController!.popToViewController(aViewController, animated: true)
                                    }
                                }
                            }))
                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in


                            }))
                            self.present(alert, animated: true, completion: nil)

                        }
                    }
                }
            }
        }
    }
}


