//
//  articleByFolderVC.swift
//  Eboni
//
//  Created by Apple on 03/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

class articleByFolderVC: UIViewController {
    
    
    var folderArticleList:[AnyObject] = []
    
    var viewModel : articleByFolderListVM!
    
    @IBOutlet weak var lblFolderNAme : UILabel!
    @IBOutlet weak var tblFolderArticleList : UITableView!
    @IBOutlet weak var viewRemoveItem : UIView!
    
    @IBOutlet weak var removeFolderPopup : RemoveItemfromFolder!
    
    var isFetching = false
    var addsFIndex = 5
    
    var scrollIndex = 1
    var totalPageCount: Int?
    
    var folderId: String = ""
    var folderName: String = ""
    
    var refrstTbl: UIRefreshControl!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var hgtFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                        
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        self.viewFooter.isHidden = true
        self.hgtFooter.constant = 0
        self.spinner.isHidden = true
        
        self.view.bringSubviewToFront(viewFooter)
        
        self.viewRemoveItem.isHidden = true
        
        self.removeFolderPopup.removefolderDelegate = self
        
        
        refrstTbl = UIRefreshControl()
        refrstTbl.addTarget(self, action: #selector(onRefreshTbl), for: .valueChanged)
        tblFolderArticleList.addSubview(refrstTbl)
        
        APIClient.showLoaderView(view: self.view)
        self.setFolderArticlesApi(strCkeck: "get")

    }
    
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension articleByFolderVC {
    
    func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    @objc func onRefreshTbl() {
        run(after: 2) {
            self.scrollIndex = 1
            self.folderArticleList = []
            APIClient.showLoaderView(view: self.view)
            self.setFolderArticlesApi(strCkeck: "get")
            self.refrstTbl.endRefreshing()
        }
    }
}

extension articleByFolderVC {
    
    func setFolderArticlesApi(strCkeck: String){
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.folderArticleList = []
        }
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        APIClient.artticleListByFolder(userId: userId, pageNo: "\(self.scrollIndex)", folderId: self.folderId) { (listByFolder) in
            
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                
                
                if let dict = listByFolder as? NSDictionary{
                    let total_no_of_page = dict["total_no_of_page"] as? Int
                    if let arrData = dict["allarticlesLists"] as? [NSDictionary]{
                        
                        if(strCkeck == "get"){
                            
                            if(arrData.count == 0){
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                            else{
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.folderArticleList.append(obj)
                                }
                                
                                self.totalPageCount = total_no_of_page
                                self.viewModel = articleByFolderListVM(controller: self, items: self.folderArticleList)
                                self.viewModel.viewContrlr = self
                                self.tblFolderArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                
                                self.tblFolderArticleList.register(UINib(nibName: "UnifiedNativeAdCell", bundle: nil),
                                                                   forCellReuseIdentifier: "UnifiedNativeAdCell")
                                
                                self.tblFolderArticleList.delegate = self
                                self.tblFolderArticleList.dataSource = self
                                
                                self.isFetching = false
                                self.tblFolderArticleList.reloadData()
                                
                            }
                        }
                        else{
                            if (arrData.count > 0)
                            {
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.folderArticleList.append(obj)
                                }
                                self.totalPageCount = total_no_of_page
                                self.viewModel = articleByFolderListVM(controller: self, items: self.folderArticleList)
                                self.viewModel.viewContrlr = self
                                self.tblFolderArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                
                                self.tblFolderArticleList.register(UINib(nibName: "UnifiedNativeAdCell", bundle: nil),
                                                                   forCellReuseIdentifier: "UnifiedNativeAdCell")
                                
                                self.tblFolderArticleList.delegate = self
                                self.tblFolderArticleList.dataSource = self
                                
                                
                                self.isFetching = false
                                self.tblFolderArticleList.reloadData()
                                
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                                
                            }
                            else{
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                        }
                    }
                }
            }
        }
    }
}

extension articleByFolderVC : removefolderDelegate {
    
    func clickOnBack() {
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.viewRemoveItem.layer.add(transition, forKey: kCATransition)
        self.viewRemoveItem.isHidden = true
        
    }
    
    
}

extension articleByFolderVC {
    
    @IBAction func clickOnBottomBtn(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 10){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}

extension articleByFolderVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblFolderArticleList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblFolderArticleList)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblFolderArticleList)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblFolderArticleList)
    }
}

extension articleByFolderVC {
    
    func openFolderPopUp(tag: Int){
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        self.viewRemoveItem.layer.add(transition, forKey: kCATransition)
        self.viewRemoveItem.isHidden = false
        
        let obj = self.viewModel.items[tag] as! NSDictionary
        let folder_name = obj["folder_name"] as? String
        
        self.removeFolderPopup.lblFolderTitle.text = "You are viewing the \(folder_name ?? "")"
        
        self.removeFolderPopup.btnRemove.tag = tag
        self.removeFolderPopup.btnRemove.addTarget(self, action: #selector(removeBtnApi(_:)), for: .touchUpInside)
        
        
    }
    
    @objc func removeBtnApi(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        let obj = self.viewModel.items[sender.tag] as! NSDictionary
        
        let userId = obj["userid"] as? String
        let folderId = obj["folder_id"] as? String
        let articleId = obj["article_id"] as? String
        
        
        APIClient.deleteArticleFromFolder(userId: userId!, folderId: folderId!, articleId: articleId!) { (response, error) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                if(response != nil){
                    let statsu = response?.status
                    if(statsu == 1){
                        self.viewWillAppear(true)
                    }
                    else{
                        APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: (response?.message!)!)
                    }
                }
                else{
                    self.viewWillAppear(true)
                }
            }
        }
    }
}



