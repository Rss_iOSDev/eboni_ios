//
//  homeViewController.swift
//  Eboni
//
//  Created by Apple on 26/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import ImageSlideshow

class homeViewController: UIViewController {
    
    var refreshControl: UIRefreshControl!
    var refreshTbl: UIRefreshControl!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBlackHistory: UILabel!
    
    @IBOutlet weak var hgtBlackView: NSLayoutConstraint!
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var btnSlider: UIButton!
    @IBOutlet weak var imgBannerHgt: NSLayoutConstraint!
    
    @IBOutlet weak var lblHeadline: UILabel!
    
    @IBOutlet weak var viewTop10: UIView!
    @IBOutlet weak var hgtTop10: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewHomeBanner: UIView!
    @IBOutlet weak var viewCatBannerAds: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var mainBanerViewHgt: NSLayoutConstraint!
    
    
    var belowTopIndex = 5
    var catIndex = 5
    
    //   @IBOutlet weak var hgtTable: NSLayoutConstraint!
    
    // var linkurl : String?
    
    /// The ad unit ID from the AdMob UI.
    //  let adUnitID = "ca-app-pub-3940256099942544/8407707713"
    
    
    /// The native ads.
    
    
    /// The spinner view.
    
    
    var temp : IndexPath?
    
    var items:[Category_info] = []
    var indexArray = [Int]()
    
    var articleList:[AllarticlesLists] = []
    
    var isNewDataLoading = false
    var isFetchingCat = false
    
    var belowArticleList_cat:[AnyObject] = []
    var belowArticleList:[AnyObject] = []
    
    var cat_Id = ""
    
    var totalPageCount: Int?
    
    var viewModel : articleListVM!
    var viewModel_cat : articleListVM!
    
    var indexSwipe = 0
    
    @IBOutlet weak var collFeatureCat: UICollectionView! {
        didSet {
            collFeatureCat.dataSource = self
            collFeatureCat.delegate = self
        }
    }
    
    @IBOutlet weak var collTopArticleList : UICollectionView!
    @IBOutlet weak var tblBelowArticleList : customTblView!
    @IBOutlet weak var tblBelowArticleListCat : UITableView!
    
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var viewScroll: UIScrollView!
    
    @IBOutlet weak var viewArticleByCat: UIView!
    
    
    @IBOutlet weak var viewCatFooter: UIView!
    @IBOutlet weak var hgtCatFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    @IBOutlet weak var scrollFooter: UIView!
    @IBOutlet weak var hgtscrollFooter: NSLayoutConstraint!
    @IBOutlet weak var scrollspinner : UIActivityIndicatorView!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    
    var scrollIndex = 1
    var scrollIndex_cat = 1
    
    var arrBannerImg = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "Home"
        
        indexArray.append(0)
        
        let result = Date().localizedDescription(dateStyle: .full, timeStyle: .none)
        self.lblHeadline.text = "\(result)"
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        viewScroll.insertSubview(refreshControl, at: 0)
        
        
        refreshTbl = UIRefreshControl()
        refreshTbl.addTarget(self, action: #selector(onRefreshTbl), for: .valueChanged)
        tblBelowArticleListCat.addSubview(refreshTbl)
        
        self.viewCatBannerAds.isHidden = true
        self.viewHomeBanner.isHidden = false
        
        
        self.viewCatFooter.isHidden = true
        self.hgtCatFooter.constant = 0
        self.spinner.isHidden = true
        
        self.scrollFooter.isHidden = true
        self.hgtscrollFooter.constant = 0
        self.scrollFooter.isHidden = true
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        self.viewArticleByCat.addGestureRecognizer(leftSwipe)
        self.viewArticleByCat.addGestureRecognizer(rightSwipe)
        
        let leftSwipe1 = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(_:)))
        let rightSwipe2 = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(_:)))
        leftSwipe1.direction = .left
        rightSwipe2.direction = .right
        self.viewScroll.addGestureRecognizer(leftSwipe1)
        self.viewScroll.addGestureRecognizer(rightSwipe2)
        
        self.loadBanner(self.bannerView)
        
        
        
        
        self.setUpUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        // Do any additional setup after loading the view.
    }
    
    
    func reFreshToken(){
        
        if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
            if let device_token = userDef.object(forKey: "device_token") as? String{
                APIClient.sendRefeshToken(device_id: device_token, refreshTokens: fcmToken) { (response) in
                    print(response)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
            }
        }
        else{
            
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        
        self.reFreshToken()
    }
    
    func setUpUI()
    {
        // let space: CGFloat = 30
        // self.collFeatureCat = UICollectionView(frame: self.view_2.frame, collectionViewLayout: collectionLayout)
        
        self.collFeatureCat?.register(UINib(nibName: "featuredCell", bundle: nil), forCellWithReuseIdentifier: "featuredCell")
        
        //  self.view_2.addSubview(self.collFeatureCat!)
        
        self.collTopArticleList.register(UINib(nibName: "topArticleCell", bundle: nil), forCellWithReuseIdentifier: "topArticleCell")
        
        self.view_1.isHidden = true
        self.view_2.isHidden = true
        
        self.viewScroll.isHidden = true
        self.viewArticleByCat.isHidden = true
        self.viewTop10.isHidden = true
        self.hgtTop10.constant = 0
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.indexArray.removeAll()
        indexArray.append(0)
        collFeatureCat.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collFeatureCat.reloadData()
        
        // APIClient.showLoaderView(view: self.view)
        
        //        viewBanner.adUnitID = Endpoints.Environment.googleAdsUniId
        //        viewBanner.rootViewController = self
        //        viewBanner.load(GADRequest())
        //
        //        viewBanner_Categories.adUnitID = Endpoints.Environment.googleAdsUniId
        //        viewBanner_Categories.rootViewController = self
        //        viewBanner_Categories.load(GADRequest())
        
        
        self.callAllApionFirstTime()
        self.setbelowArticlesApi(strCkeck: "get")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
    }
}

extension homeViewController {
    
    func animationDuration() -> Double {
        return 0.5
    }
    
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        let duration = animationDuration()
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        Analytics.logEvent("Feature_List", parameters: [
            "userid": userId
        ])
        
        
        if (sender.direction == .left) {
            print("Swipe Left")
            
            if(indexSwipe < self.items.count){
                if(indexSwipe == self.items.count - 1){
                }
                else{
                    
                    indexSwipe += 1
                    indexArray.removeAll()
                    indexArray.append(indexSwipe)
                    let obj = self.items[indexSwipe]
                    
                    self.viewCatBannerAds.isHidden = false
                    self.viewHomeBanner.isHidden = true
                    self.mainBanerViewHgt.constant = 50
                    
                    
                    lblTitle.text = obj.cat_name
                    self.cat_Id = obj.cat_id!
                    self.belowArticleList_cat.removeAll()
                    self.scrollIndex_cat = 1
                    APIClient.showLoaderView(view: self.view)
                    self.setbelowArticlesApiWithCatId(strCkeck: "get", cat_id: self.cat_Id)
                    
                    UIView.animate(withDuration: duration, animations: {
                        let labelPosition = CGPoint(x: self.viewArticleByCat.frame.origin.x - self.viewArticleByCat.frame.size.width, y: self.viewArticleByCat.frame.origin.y)
                        print(labelPosition)
                        self.viewArticleByCat.frame = CGRect(x: labelPosition.x, y: labelPosition.y, width: self.viewArticleByCat.frame.size.width, height: self.viewArticleByCat.frame.size.height)
                        
                        self.collFeatureCat.scrollToItem(at: IndexPath(item: self.indexSwipe, section: 0), at: .centeredHorizontally, animated: true)
                        self.collFeatureCat.reloadData()
                        
                    })
                }
            }
        }
        
        if (sender.direction == .right) {
            print("Swipe Right")
            if indexSwipe > 0 {
                indexSwipe -= 1
                
                indexArray.removeAll()
                indexArray.append(indexSwipe)
                let obj = self.items[indexSwipe]
                if(indexSwipe == 0){
                    lblTitle.text = "Home"
                    self.cat_Id = ""
                    self.setUpUI()
                    
                    
                    self.viewCatBannerAds.isHidden = true
                    self.viewHomeBanner.isHidden = false
                    
                    UIView.animate(withDuration: duration, animations: {
                        let labelPosition = CGPoint(x: self.viewScroll.frame.origin.x + self.viewScroll.frame.size.width, y: self.viewScroll.frame.origin.y)
                        self.viewScroll.frame = CGRect(x: labelPosition.x, y: labelPosition.y, width: self.viewScroll.frame.size.width, height: self.viewScroll.frame.size.height)
                    })
                    self.collFeatureCat.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                    self.collFeatureCat.reloadData()
                }
                else{
                    
                    
                    self.viewCatBannerAds.isHidden = false
                    self.viewHomeBanner.isHidden = true
                    self.mainBanerViewHgt.constant = 50
                    
                    lblTitle.text = obj.cat_name
                    self.cat_Id = obj.cat_id!
                    self.belowArticleList_cat.removeAll()
                    self.scrollIndex_cat = 1
                    APIClient.showLoaderView(view: self.view)
                    self.setbelowArticlesApiWithCatId(strCkeck: "get", cat_id: self.cat_Id)
                }
                
                UIView.animate(withDuration: duration, animations: {
                    let labelPosition = CGPoint(x: self.viewArticleByCat.frame.origin.x + self.viewArticleByCat.frame.size.width, y: self.viewArticleByCat.frame.origin.y)
                    self.viewArticleByCat.frame = CGRect(x: labelPosition.x, y: labelPosition.y, width: self.viewArticleByCat.frame.size.width, height: self.viewArticleByCat.frame.size.height)
                    
                    self.collFeatureCat.scrollToItem(at: IndexPath(item: self.indexSwipe, section: 0), at: .centeredHorizontally, animated: true)
                    self.collFeatureCat.reloadData()
                })
            }
        }
    }
    
}

//group call
extension homeViewController {
    
    func callAllApionFirstTime(){
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()   // <<---
        APIClient.categoryList(userId: userId) {(catModel) in
            
            OperationQueue.main.addOperation { [self] in
                self.items = []
                
                let arrData = catModel
                if(arrData.count > 0){
                    for i in 0..<arrData.count{
                        let obj = arrData[i]
                        self.items.append(obj)
                    }
                    self.collFeatureCat?.reloadData()
                    
                }
                dispatchGroup.leave()   // <<----
                
            }
        }
        
        dispatchGroup.enter()   // <<---
        APIClient.bannerApi() { (response) in
            OperationQueue.main.addOperation {
                if let resp = response as? NSDictionary{
                    
                    var tt = 20.0
                    
                    if let banner_speed_time = resp["banner_speed_time"] as? String{
                        tt = Double(banner_speed_time) ?? 20.0
                        self.slideshow.slideshowInterval = tt
                    }
                    else{
                        self.slideshow.slideshowInterval = tt
                    }
                    
                    self.slideshow.contentScaleMode = .scaleAspectFill
                    self.slideshow.activityIndicator = DefaultActivityIndicator()
                    
                    let pageControl = UIPageControl()
                    pageControl.currentPageIndicatorTintColor = UIColor.clear
                    pageControl.pageIndicatorTintColor = UIColor.clear
                    self.slideshow.pageIndicator = pageControl
                    
                    
                    if let homepagbannerdata = resp["homepagbannerdata"] as? [NSDictionary]{
                        print(homepagbannerdata)
                        if(homepagbannerdata.count > 0){
                            for i in 0..<homepagbannerdata.count{
                                let dictA = homepagbannerdata[i]
                                self.arrBannerImg.add(dictA)
                            }
                            var arrIn = [InputSource]()
                            for i in 0..<self.arrBannerImg.count{
                                let dict = self.arrBannerImg[i] as! NSDictionary
                                let banner_image = dict["ios_banner_image"] as! String
                                arrIn.append(SDWebImageSource(urlString: banner_image)!)
                            }
                            let dict = self.arrBannerImg[0] as! NSDictionary
                            let banner_image = dict["ios_banner_image"] as! String
                            let cellFrame = self.slideshow.frame.size
                            self.imgBanner.sd_setImage(with: URL.init(string: banner_image), placeholderImage: nil, options: [], completed: { (theImage, error, cache, url) in
                                self.imgBannerHgt.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame,downloadedImage: theImage!)
                                
                                self.mainBanerViewHgt.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame,downloadedImage: theImage!)
                                
                                self.slideshow.setImageInputs(arrIn)
                            })
                        }
                    }
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.enter()
        APIClient.blackHistoryInfo { (blackModel) in
            OperationQueue.main.addOperation {
                let obj = blackModel.blackHistory_info
                let str = obj?.summery
                self.lblBlackHistory.text = str?.htmlToString
                self.lblBlackHistory.sizeToFit()
                dispatchGroup.leave()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            // your code here
            self.view_1.isHidden = false
            self.view_2.isHidden = false
            self.viewScroll.isHidden = false
            self.setTop10Articles()
        }
        
        dispatchGroup.notify(queue: .main) {
            // whatever you want to do when both are done
        }
    }
    
    func setTop10Articles(){
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        APIClient.top10ArticleListApi(userId: userId) { (articleModel) in
            OperationQueue.main.addOperation {
                let arrData = articleModel.allarticlesLists!
                for i in 0..<arrData.count{
                    let obj = arrData[i]
                    self.articleList.append(obj)
                }
                if self.articleList.count == 0{
                    self.viewTop10.isHidden = true
                    self.hgtTop10.constant = 0
                }else{
                    self.viewTop10.isHidden = false
                    if UIDevice.current.userInterfaceIdiom == .phone {
                        self.hgtTop10.constant = 400
                    }
                    else if UIDevice.current.userInterfaceIdiom == .pad {
                        
                        let deviceType = UIDevice().type
                        print(deviceType)
                        if(deviceType == .iPadPro12_9 || deviceType == .iPadPro2_12_9 || deviceType == .iPadPro3_12_9 || deviceType == .iPadPro4_12_9)
                        {
                            self.hgtTop10.constant = 780
                        }
                        else {
                            self.hgtTop10.constant = 680
                        }
                    }
                    self.collTopArticleList.reloadData()
                }
                
                //                DispatchQueue.main.asyncAfter(deadline: .now()){
                //                    // your code here
                //                    self.setbelowArticlesApi(strCkeck: "get")
                //                }
            }
        }
    }
}

extension homeViewController {
    
    @objc func onRefresh() {
        
        run(after: 2) {
            //   self.viewWillAppear(true)
            self.viewScroll.isHidden = true
            self.viewArticleByCat.isHidden = true
            self.viewTop10.isHidden = true
            self.hgtTop10.constant = 0
            
            self.callAllApionFirstTime()
            self.setbelowArticlesApi(strCkeck: "get")
            
            self.refreshControl.endRefreshing()
        }
    }
    
    func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    @objc func onRefreshTbl() {
        
        run(after: 2) {
            self.scrollIndex_cat = 1
            self.belowArticleList_cat.removeAll()
            APIClient.showLoaderView(view: self.view)
            self.setbelowArticlesApiWithCatId(strCkeck: "get", cat_id: self.cat_Id)
            
            self.refreshTbl.endRefreshing()
        }
    }
}

extension homeViewController {
    
    
    func setbelowArticlesApi(strCkeck: String){
        
        isNewDataLoading = false
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.belowArticleList = []
        }
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        print(self.scrollIndex)
        
        APIClient.belowArticleListApi(userId: userId, pageNo: "\(self.scrollIndex)") { (belowModel) in
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                // let arrData = belowModel.allarticlesLists!
                
                print(belowModel)
                
                if let dict = belowModel as? NSDictionary{
                    
                    let total_no_of_page = dict["total_no_of_page"] as? Int
                    
                    if let arrData = dict["allarticlesLists"] as? [NSDictionary]{
                        
                        if(strCkeck == "get"){
                            if(arrData.count == 0){
                                self.scrollFooter.isHidden = true
                                self.hgtscrollFooter.constant = 0
                                self.scrollFooter.isHidden = true
                            }
                            else{
                                
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.belowArticleList.append(obj)
                                }
                                
                                self.totalPageCount = total_no_of_page ?? 0
                                
                                self.view_1.isHidden = false
                                self.view_2.isHidden = false
                                self.viewArticleByCat.isHidden = true
                                
                                for i in 0..<self.belowArticleList.count{
                                  //  for nativeAd in self.banenrAds {
                                        if(i == self.belowTopIndex){
                                            self.belowArticleList.insert("Insert_banner" as AnyObject, at: self.belowTopIndex)
                                            self.belowTopIndex += 6
                                           // self.nativeAds.append(nativeAd)
                                        }
                                    //}
                                }
                                
                                self.viewModel = articleListVM(controller: self, items: self.belowArticleList)
                                self.viewModel.vc = self
                                self.viewModel.viewContoller = self
                                self.viewModel.typeVC = "home"
                                self.tblBelowArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                self.tblBelowArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                  forCellReuseIdentifier: "bannerAdsCell")
                                self.tblBelowArticleList.delegate = self
                                self.tblBelowArticleList.dataSource = self
                                
                                self.tblBelowArticleList.reloadData()
                                self.tblBelowArticleList.layoutIfNeeded()
                                
                            }
                        }
                        else{
                            
                            if (arrData.count > 0)
                            {
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.belowArticleList.append(obj)
                                }
                                
                                self.totalPageCount = total_no_of_page ?? 0
                                
                                self.view_1.isHidden = false
                                self.view_2.isHidden = false
                                self.viewArticleByCat.isHidden = true
                                
                                for i in 0..<self.belowArticleList.count{
                                  //  for nativeAd in self.banenrAds {
                                        if(i == self.belowTopIndex){
                                            self.belowArticleList.insert("Insert_banner" as AnyObject, at: self.belowTopIndex)
                                            self.belowTopIndex += 6
                                           // self.nativeAds.append(nativeAd)
                                        }
                                    //}
                                }
                                
//                                for i in 0..<self.belowArticleList.count{
//                                    for nativeAd in self.nativeAds {
//                                        if(i == self.belowTopIndex){
//                                            self.belowArticleList.insert(nativeAd, at: self.belowTopIndex)
//                                            self.belowTopIndex += 6
//                                            self.nativeAds.append(nativeAd)
//                                        }
//                                    }
//                                }
                                
                                self.viewModel = articleListVM(controller: self, items: self.belowArticleList)
                                self.viewModel.vc = self
                                self.viewModel.typeVC = "home"
                                self.viewModel.viewContoller = self
                                self.tblBelowArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                
                                self.tblBelowArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                  forCellReuseIdentifier: "bannerAdsCell")
                                
                                
                                self.tblBelowArticleList.delegate = self
                                self.tblBelowArticleList.dataSource = self
                                
                                self.tblBelowArticleList.reloadData()
                                self.tblBelowArticleList.layoutIfNeeded()
                                self.scrollFooter.isHidden = true
                                self.hgtscrollFooter.constant = 0
                                self.scrollFooter.isHidden = true
                            }
                            else{
                                self.scrollFooter.isHidden = true
                                self.hgtscrollFooter.constant = 0
                                self.scrollFooter.isHidden = true
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    func setbelowArticlesApiWithCatId(strCkeck: String, cat_id: String){
        self.viewScroll.isHidden = true
        
        if(strCkeck == "get"){
            scrollIndex_cat = 1
            self.belowArticleList_cat = []
        }
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        APIClient.belowArticlebyCatId(userId: userId, pageNo: "\(self.scrollIndex_cat)", cat_id: cat_id) { (belowModel) in
            
            OperationQueue.main.addOperation{
                APIClient.hideLoaderView(view: self.view)
                
                if let dict = belowModel as? NSDictionary{
                    
                    let total_no_of_page = dict["total_no_of_page"] as? Int
                    
                    if let arrData = dict["allarticlesLists"] as? [NSDictionary]{
                        
                        if(strCkeck == "get"){
                            if(arrData.count == 0){
                                self.viewArticleByCat.isHidden = true
                                self.viewCatFooter.isHidden = true
                                self.hgtCatFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                            else{
                                
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.belowArticleList_cat.append(obj)
                                }
                                
                                self.totalPageCount = total_no_of_page
                                self.view_1.isHidden = false
                                self.view_2.isHidden = false
                                
                                for i in 0..<self.belowArticleList_cat.count{
                                  //  for nativeAd in self.banenrAds {
                                        if(i == self.catIndex){
                                            self.belowArticleList_cat.insert("Insert_banner" as AnyObject, at: self.catIndex)
                                            self.catIndex += 6
                                           // self.nativeAds.append(nativeAd)
                                        }
                                    //}
                                }
                                
                                
//                                for i in 0..<self.belowArticleList_cat.count{
//                                    for nativeAd in self.nativeAds {
//                                        if(i == self.catIndex){
//                                            self.belowArticleList_cat.insert(nativeAd, at: self.catIndex)
//                                            self.catIndex += 6
//                                            self.nativeAds.append(nativeAd)
//                                        }
//                                    }
//                                }
                                
                                
                                self.viewModel_cat = articleListVM(controller: self, items: self.belowArticleList_cat)
                                self.viewModel_cat.vc = self
                                self.viewModel_cat.viewContoller = self
                                self.viewModel_cat.typeVC = "home_cat"
                                self.viewModel_cat.cat_id = cat_id
                                self.viewModel_cat.isDataLoad = false
                                self.tblBelowArticleListCat.register(UINib(nibName: self.viewModel_cat.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel_cat.identifierItem)
                                
                                self.tblBelowArticleListCat.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                     forCellReuseIdentifier: "bannerAdsCell")
                                
                                
                                self.tblBelowArticleListCat.delegate = self
                                self.tblBelowArticleListCat.dataSource = self
                                
                                self.viewArticleByCat.isHidden = false
                                self.isFetchingCat = false
                                
                                self.tblBelowArticleListCat.reloadData()
                                
                                if self.viewModel_cat.items.count == 10{
                                    let indexPath = NSIndexPath(row: 0, section: 0)
                                    self.tblBelowArticleListCat.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                }
                            }
                        }
                        else{
                            
                            if (arrData.count > 0)
                            {
                                
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.belowArticleList_cat.append(obj)
                                }
                                self.totalPageCount = total_no_of_page
                                self.view_1.isHidden = false
                                self.view_2.isHidden = false
                                
                                for i in 0..<self.belowArticleList_cat.count{
                                  //  for nativeAd in self.banenrAds {
                                        if(i == self.catIndex){
                                            self.belowArticleList_cat.insert("Insert_banner" as AnyObject, at: self.catIndex)
                                            self.catIndex += 6
                                           // self.nativeAds.append(nativeAd)
                                        }
                                    //}
                                }
                                
                                self.viewModel_cat = articleListVM(controller: self, items: self.belowArticleList_cat)
                                self.viewModel_cat.vc = self
                                self.viewModel_cat.viewContoller = self
                                self.viewModel_cat.typeVC = "home_cat"
                                self.viewModel_cat.cat_id = cat_id
                                self.viewModel_cat.isDataLoad = false
                                
                                self.tblBelowArticleListCat.register(UINib(nibName: self.viewModel_cat.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel_cat.identifierItem)
                                self.tblBelowArticleListCat.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                     forCellReuseIdentifier: "bannerAdsCell")
                                
                                self.tblBelowArticleListCat.delegate = self
                                self.tblBelowArticleListCat.dataSource = self
                                
                                self.viewArticleByCat.isHidden = false
                                self.isFetchingCat = false
                                
                                self.tblBelowArticleListCat.reloadData()
                                
                                if self.viewModel_cat.items.count == 10{
                                    let indexPath = NSIndexPath(row: 0, section: 0)
                                    self.tblBelowArticleListCat.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                                }
                                self.viewCatFooter.isHidden = true
                                self.hgtCatFooter.constant = 0
                                self.spinner.isHidden = true
                                
                            }
                            else{
                                self.spinner.isHidden = true
                                self.viewCatFooter.isHidden = true
                                self.hgtCatFooter.constant = 0
                            }
                        }
                    }
                }
            }
        }
    }
    
}

extension homeViewController {
    
    func callApiWhenClickonSeeAllBtn(category_id: String, cat_name: String){
        
        self.viewArticleByCat.isHidden = true
        self.viewScroll.isHidden = true
        
        self.scrollIndex_cat = 1
        self.belowArticleList_cat = []
        
        lblTitle.text = cat_name
        self.cat_Id = category_id
        
        indexArray.removeAll()
        
        if(self.items.count > 0){
            for i in 0..<self.items.count{
                let obj = self.items[i]
                let cactId = obj.cat_id
                if(category_id == cactId){
                    self.indexSwipe = i
                    indexArray.append(i)
                    break
                }
            }
            
            self.collFeatureCat.scrollToItem(at:IndexPath(item: self.indexSwipe, section: 0), at: .left, animated: true)
            self.collFeatureCat.reloadData()
        }
        APIClient.showLoaderView(view: self.view)
        self.setbelowArticlesApiWithCatId(strCkeck: "get", cat_id: self.cat_Id)
        
    }
}

extension homeViewController {
    
    @IBAction func clickOnBanner(_ sender: UIButton)
    {
        if(slideshow.currentPage == 0){
            let detail = self.storyboard?.instantiateViewController(identifier: "shareInviteViewController") as! shareInviteViewController
            detail.modalPresentationStyle = .overCurrentContext
            self.present(detail, animated: true, completion: nil)
        }
        else if(slideshow.currentPage == 1){
            let dict = self.arrBannerImg[1] as! NSDictionary
            let second_banner_link = dict["ios_link_url"] as! String
            guard let url = URL(string: second_banner_link) else { return }
            UIApplication.shared.open(url)
        }
        else if(slideshow.currentPage == 2){
            let dict = self.arrBannerImg[2] as! NSDictionary
            let ios_link_url = dict["third_banner_link"] as! String
            guard let url = URL(string: ios_link_url) else { return }
            UIApplication.shared.open(url)
        }
    }
}

extension homeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.tblBelowArticleList){
            return viewModel.getNumbersOfRows(in: section)
        }
        else{
            return viewModel_cat.getNumbersOfRows(in: section)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == self.tblBelowArticleList){
            return viewModel.getCellForRowAt(indexPath, tableView: tblBelowArticleList)
        }
        else{
            return viewModel_cat.getCellForRowAt(indexPath, tableView: tblBelowArticleListCat)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == self.tblBelowArticleList){
            return viewModel.getHeightForRowAt(indexPath, tableView: tblBelowArticleList)
        }
        else{
            return viewModel_cat.getHeightForRowAt(indexPath, tableView: tblBelowArticleListCat)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(tableView == self.tblBelowArticleList){
        }
        else{
            return viewModel_cat.willDisplayCell(indexPath, tableView: tblBelowArticleListCat)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == self.tblBelowArticleList){
            return viewModel.didSelectRowAt(indexPath, tableView: tblBelowArticleList)
        }
        else{
            return viewModel_cat.didSelectRowAt(indexPath, tableView: tblBelowArticleListCat)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(scrollView == viewScroll){
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if self.isNewDataLoading == false{
                    self.isNewDataLoading = true
                    self.scrollFooter.isHidden = false
                    self.hgtscrollFooter.constant = 45
                    self.scrollFooter.isHidden = false
                    self.perform(#selector(loadData), with: nil, afterDelay: 1)
                }
            }
        }
    }
    
    @objc func loadData(){
        self.scrollIndex += 1
        self.setbelowArticlesApi(strCkeck: "load")
    }
}

extension homeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(collectionView == collFeatureCat){
            return 1
        }
        if(collectionView == collTopArticleList){
            return 1
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == collFeatureCat){
            return self.items.count
        }
        if(collectionView == collTopArticleList){
            return self.articleList.count
        }
        else{
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if(collectionView == collFeatureCat){
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredCell", for: indexPath) as? featuredCell else { return UICollectionViewCell() }
            
            let obj = self.items[indexPath.row]
            cell.lblName.text = obj.cat_name
            
            if(indexArray.contains(indexPath.row)){
                cell.lblName.textColor = Colors.THEME_COLOR
                cell.lblLine.backgroundColor = Colors.THEME_COLOR
                
            }
            else{
                cell.lblName.textColor = Colors.GRAY_COLOR
                cell.lblLine.backgroundColor = UIColor.clear
            }
            
            return cell
        }
        
        if(collectionView == collTopArticleList){
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topArticleCell", for: indexPath) as? topArticleCell else { return UICollectionViewCell() }
            let obj = self.articleList[indexPath.row]
            
            cell.viewBookmark.roundCorners([.bottomLeft, .bottomRight], radius: 8)
            
            let img_url = obj.field_image_url_value
            cell.imgNews.sd_setImage(with: URL.init(string: img_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))
            
            cell.imgNews.clipsToBounds = true
            cell.imgNews.layer.cornerRadius = 12
            cell.imgNews.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
            if let mode = userDef.value(forKey: "isMode") as? String
            {
                if(mode == "light"){
                    cell.imgFolder.image = UIImage(named: "addFolderIcon")
                }
                else if(mode == "dark"){
                    cell.imgFolder.image = UIImage(named: "addFolderIconDark")
                }
                else if(mode == "auto"){
                    if #available(iOS 13.0, *) {
                        if UITraitCollection.current.userInterfaceStyle == .dark {
                            cell.imgFolder.image = UIImage(named: "addFolderIconDark")
                        }
                        else {
                            cell.imgFolder.image = UIImage(named: "addFolderIcon")
                        }
                    }
                }
            }
            else{
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        cell.imgFolder.image = UIImage(named: "addFolderIconDark")
                    }
                    else {
                        cell.imgFolder.image = UIImage(named: "addFolderIcon")
                    }
                }
            }
            
            let pgCount = "\(indexPath.row+1)"
            if(pgCount == "10"){
                cell.lblPageCount.text = "\(pgCount)"
            }
            else{
                cell.lblPageCount.text = "0\(pgCount)"
            }
            
            cell.lblFiledValue.text = obj.field_web_name_value
            
            cell.lblCreate.text = obj.created
            cell.lblTitle.text = obj.title
            
            let publis_url = obj.publisher_logo
            cell.imgPublisher.sd_setImage(with: URL.init(string: publis_url!), placeholderImage: UIImage(named: "newsImgPlaceholder"))
            
            cell.btnPublish.tag = indexPath.row
            cell.btnPublish.addTarget(self, action: #selector(self.imageTappedonTop(_:)), for: .touchUpInside)
            
            cell.btnFolder.tag = indexPath.row
            cell.btnFolder.addTarget(self, action: #selector(self.clickOnFolder(_:)), for: .touchUpInside)
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(self.clickOnTopShare(_:)), for: .touchUpInside)
            
            let likeC = obj.total_likes
            cell.lblLikeCount.text = "\(likeC ?? "0")"
            
            
            let fav = obj.isFavorite ?? "0"
            if(fav == "0"){
                cell.imgLikes.image = UIImage(named: "unlike")
            }else{
                cell.imgLikes.image = UIImage(named: "like")
            }
            
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.clickOnTop10Like(_:)), for: .touchUpInside)
            
            
            return cell
            
        }
        return cell
    }
    
    @objc func imageTappedonTop(_ sender: UIButton) {
        let obj = self.articleList[sender.tag]
        self.passToPublisherVC(strUrl: obj.publisher_id!)
    }
    
    
    
    @objc func clickOnFolder(_ sender: UIButton)
    {
        if(userGlobal?.id == nil){
            let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
            detail.modalPresentationStyle = .overCurrentContext
            detail.vc = self
            let navC = UINavigationController(rootViewController: detail)
            navC.navigationBar.isHidden = true
            self.present(navC, animated: true, completion: nil)

            //self.present(detail, animated: true, completion: nil)
        }
        else{
            let obj = self.articleList[sender.tag]
            let detail = self.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
            detail.modalPresentationStyle = .overCurrentContext
            detail.articleId = obj.nid!
            detail.openFrom = "add"
            self.present(detail, animated: true, completion: nil)
        }
    }
    
    @objc func clickOnTopShare(_ sender: UIButton)
    {
        let obj = self.articleList[sender.tag]
        let detail = self.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
        detail.modalPresentationStyle = .overCurrentContext
        detail.strLink = obj.full_url!
        detail.nid = obj.nid!
        detail.strTitle = obj.title!
        detail.nodeAlias = obj.nodealias!
        self.present(detail, animated: true, completion: nil)
        
    }
    
    @objc func clickOnTop10Like(_ sender: UIButton)
    {
        if(userGlobal?.id == nil){
            let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
            detail.modalPresentationStyle = .overCurrentContext
            detail.vc = self
            let navC = UINavigationController(rootViewController: detail)
            navC.navigationBar.isHidden = true
            self.present(navC, animated: true, completion: nil)

        }
        else{
            
            var obj = self.articleList[sender.tag]
            let buttonPosition = sender.convert(CGPoint.zero, to: self.collTopArticleList)
            let indexPath = self.collTopArticleList.indexPathForItem(at: buttonPosition)
            let cell = self.collTopArticleList.cellForItem(at: indexPath!) as! topArticleCell
            
            let fav = obj.isFavorite ?? "0"
            let likeC = obj.total_likes
            if(fav == "0"){
                obj.isFavorite = "1"
                var count = Int(likeC ?? "0")
                count! += 1
                cell.lblLikeCount.text = "\(count ?? 0)"
                obj.total_likes = "\(count ?? 0)"
                
            }
            else{
                obj.isFavorite = "0"
                var count = Int(likeC ?? "0")
                if(count! > 0){
                    count! -= 1
                }else{
                    count = 0
                }
                cell.lblLikeCount.text = "\(count ?? 0)"
                obj.total_likes = "\(count ?? 0)"
            }
            
            self.articleList.remove(at: sender.tag)
            self.articleList.insert(obj, at: sender.tag)
            
            self.collTopArticleList.reloadData()
            
            var userId = ""
            if(userGlobal?.id == nil){
                userId = "0"
            }
            else{
                userId = (userGlobal?.id)!
            }
            
            APIClient.likeonArticle(userId: userId, node_id: obj.nid!, likes: "1") { (resp) in
                DispatchQueue.main.async {
                    self.collTopArticleList.reloadData()
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(collectionView == collFeatureCat){
            
            var userId = ""
            if(userGlobal?.id == nil){
                userId = "0"
            }
            else{
                userId = (userGlobal?.id)!
            }
            Analytics.logEvent("Feature_List", parameters: [
                "userid": userId
            ])
            
            indexArray.removeAll()
            
            indexSwipe = indexPath.row
            
            indexArray.append(indexPath.row)
            let obj = self.items[indexPath.row]
            if(indexPath.row == 0){
                lblTitle.text = "Home"
                
                self.viewCatBannerAds.isHidden = true
                self.viewHomeBanner.isHidden = false
                
                
                self.cat_Id = ""
                self.setUpUI()
            }
            else{
                
                self.viewCatBannerAds.isHidden = false
                self.viewHomeBanner.isHidden = true
                self.mainBanerViewHgt.constant = 50
                
                lblTitle.text = obj.cat_name
                self.cat_Id = obj.cat_id!
                
                self.belowArticleList_cat.removeAll()
                self.scrollIndex_cat = 1
                // self.tblBelowArticleListCat.isHidden = true
                self.viewArticleByCat.isHidden = true
                APIClient.showLoaderView(view: self.view)
                self.setbelowArticlesApiWithCatId(strCkeck: "get", cat_id: self.cat_Id)
            }
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            collectionView.reloadData()
        }
        else if(collectionView == collTopArticleList){
            let obj = self.articleList[indexPath.row]
            if(obj.full_url != ""){
                self.passToViewController(strUrl: obj.full_url!, strArticleId: obj.nid!, strTitle: obj.title!, strFav: obj.isFavorite!, strLikeC: obj.total_likes ?? "0", strAlias: "/")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == collFeatureCat){
            let label = UILabel(frame: CGRect.zero)
            let obj = self.items[indexPath.row]
            label.text = obj.cat_name
            label.sizeToFit()
            
            var itemWidth : CGFloat = 0.0
            
            if UIDevice.current.userInterfaceIdiom == .phone {
                itemWidth = label.frame.width + 40
            }
            else if UIDevice.current.userInterfaceIdiom == .pad {
                itemWidth = label.frame.width + 60
            }
            return CGSize(width: itemWidth  , height: 55)
        }
        else if(collectionView == collTopArticleList){
            
            var itemHeight : CGFloat = 0.0
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                itemHeight = 345
            }
            
            else if UIDevice.current.userInterfaceIdiom == .pad{
                let deviceType = UIDevice().type
                print(deviceType)
                if(deviceType == .iPadPro12_9 || deviceType == .iPadPro2_12_9 || deviceType == .iPadPro3_12_9 || deviceType == .iPadPro4_12_9)
                {
                    itemHeight = 700
                }
                else {
                    itemHeight = 600
                }
            }
            return CGSize(width: collTopArticleList.frame.width - 40, height: itemHeight)
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
}

extension homeViewController{
    func passToViewController(strUrl : String, strArticleId : String, strTitle: String, strFav: String, strLikeC: String, strAlias: String){
        let detail = self.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
        detail.passUrl = strUrl
        detail.passArticleId = strArticleId
        detail.passTitle = strTitle
        detail.isFav = strFav
        detail.nodealias = strAlias
        detail.likeCount = strLikeC
        
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    func passToPublisherVC(strUrl : String){
        let detail = self.storyboard?.instantiateViewController(identifier: "publisherListViewController") as! publisherListViewController
        detail.publishrId = strUrl
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

extension homeViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 10){
            
            indexArray.removeAll()
            indexArray.append(0)
            
            // self.viewCatBannerAds.isHidden = true
            //self.viewHomeBanner.isHidden = false
            
            
            self.slideshow.isHidden = false
            self.btnSlider.isHidden = false
            
            lblTitle.text = "Home"
            self.cat_Id = ""
            
            var userId = ""
            if(userGlobal?.id == nil){
                userId = "0"
            }
            else{
                userId = (userGlobal?.id)!
            }
            Analytics.logEvent("rss_icon_tap", parameters: [
                "userid": userId
            ])
            let seach = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(seach, animated: false)
        }
    }
    
    @IBAction func clickOnSearchBtn(_ sender: UIButton)
    {
        let seach = self.storyboard?.instantiateViewController(identifier: "searchListViewController") as! searchListViewController
        self.navigationController?.pushViewController(seach, animated: true)
    }
}


class customTblView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}

extension UIViewController {
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        let widthOffset = downloadedImage.size.width - cellImageFrame.width
        let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
        let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
        let effectiveHeight = downloadedImage.size.height - heightOffset
        return(effectiveHeight)
    }
    // MARK: Optional function for resize of image
    func resizeHighImage(image:UIImage)->UIImage {
        let size = image.size.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: .zero, size: size))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }
}

extension UIViewController {
    
    @objc func addforNotification(_ notification: Notification){
        
        let _ = (notification.object as? NSDictionary)
        
        NotificationCenter.default.post(name: .didCloseSession, object: nil)
    }
    
    
    @objc func matchforNotification(_ notification: Notification){
        
        
        let nid = (notification.object as? NSDictionary)?.value(forKey: "nid") as! String
        let full_url = (notification.object as? NSDictionary)?.value(forKey: "full_url") as! String
        
        let strFav = (notification.object as? NSDictionary)?.value(forKey: "isFavorite") as! String
        let strLikeC = (notification.object as? NSDictionary)?.value(forKey: "total_likes") as! String
        
        let strAlias = (notification.object as? NSDictionary)?.value(forKey: "nodealias") as! String
        
        let strTitle = (notification.object as? NSDictionary)?.value(forKey: "title") as! String
        
        let detail = self.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
        detail.passUrl = full_url
        detail.passArticleId = nid
        detail.isFav = strFav
        detail.likeCount = strLikeC
        detail.nodealias = strAlias
        detail.passTitle = strTitle
        self.navigationController?.pushViewController(detail, animated: false)
        NotificationCenter.default.post(name: .didCloseSession, object: nil)
        
    }
    
    @objc func didCloseSession(_ notification: Notification){
        NotificationCenter.default.removeObserver(self)
    }
}




public enum Model : String {
    
    //Simulator
    case simulator     = "simulator/sandbox",
         
         //iPod
         iPod1              = "iPod 1",
         iPod2              = "iPod 2",
         iPod3              = "iPod 3",
         iPod4              = "iPod 4",
         iPod5              = "iPod 5",
         iPod6              = "iPod 6",
         iPod7              = "iPod 7",
         
         //iPad
         iPad2              = "iPad 2",
         iPad3              = "iPad 3",
         iPad4              = "iPad 4",
         iPadAir            = "iPad Air ",
         iPadAir2           = "iPad Air 2",
         iPadAir3           = "iPad Air 3",
         iPadAir4           = "iPad Air 4",
         iPad5              = "iPad 5", //iPad 2017
         iPad6              = "iPad 6", //iPad 2018
         iPad7              = "iPad 7", //iPad 2019
         iPad8              = "iPad 8", //iPad 2020
         
         //iPad Mini
         iPadMini           = "iPad Mini",
         iPadMini2          = "iPad Mini 2",
         iPadMini3          = "iPad Mini 3",
         iPadMini4          = "iPad Mini 4",
         iPadMini5          = "iPad Mini 5",
         
         //iPad Pro
         iPadPro9_7         = "iPad Pro 9.7\"",
         iPadPro10_5        = "iPad Pro 10.5\"",
         iPadPro11          = "iPad Pro 11\"",
         iPadPro2_11        = "iPad Pro 11\" 2nd gen",
         iPadPro12_9        = "iPad Pro 12.9\"",
         iPadPro2_12_9      = "iPad Pro 2 12.9\"",
         iPadPro3_12_9      = "iPad Pro 3 12.9\"",
         iPadPro4_12_9      = "iPad Pro 4 12.9\"",
         
         //iPhone
         iPhone4            = "iPhone 4",
         iPhone4S           = "iPhone 4S",
         iPhone5            = "iPhone 5",
         iPhone5S           = "iPhone 5S",
         iPhone5C           = "iPhone 5C",
         iPhone6            = "iPhone 6",
         iPhone6Plus        = "iPhone 6 Plus",
         iPhone6S           = "iPhone 6S",
         iPhone6SPlus       = "iPhone 6S Plus",
         iPhoneSE           = "iPhone SE",
         iPhone7            = "iPhone 7",
         iPhone7Plus        = "iPhone 7 Plus",
         iPhone8            = "iPhone 8",
         iPhone8Plus        = "iPhone 8 Plus",
         iPhoneX            = "iPhone X",
         iPhoneXS           = "iPhone XS",
         iPhoneXSMax        = "iPhone XS Max",
         iPhoneXR           = "iPhone XR",
         iPhone11           = "iPhone 11",
         iPhone11Pro        = "iPhone 11 Pro",
         iPhone11ProMax     = "iPhone 11 Pro Max",
         iPhoneSE2          = "iPhone SE 2nd gen",
         iPhone12Mini       = "iPhone 12 Mini",
         iPhone12           = "iPhone 12",
         iPhone12Pro        = "iPhone 12 Pro",
         iPhone12ProMax     = "iPhone 12 Pro Max",
         
         // Apple Watch
         AppleWatch1         = "Apple Watch 1gen",
         AppleWatchS1        = "Apple Watch Series 1",
         AppleWatchS2        = "Apple Watch Series 2",
         AppleWatchS3        = "Apple Watch Series 3",
         AppleWatchS4        = "Apple Watch Series 4",
         AppleWatchS5        = "Apple Watch Series 5",
         AppleWatchSE        = "Apple Watch Special Edition",
         AppleWatchS6        = "Apple Watch Series 6",
         
         //Apple TV
         AppleTV1           = "Apple TV 1gen",
         AppleTV2           = "Apple TV 2gen",
         AppleTV3           = "Apple TV 3gen",
         AppleTV4           = "Apple TV 4gen",
         AppleTV_4K         = "Apple TV 4K",
         
         unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#
// MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    
    var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }
        }
        
        let modelMap : [String: Model] = [
            
            //Simulator
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPod7,1"   : .iPod6,
            "iPod9,1"   : .iPod7,
            
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad6,11"  : .iPad5, //iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //iPad 2018
            "iPad7,6"   : .iPad6,
            "iPad7,11"  : .iPad7, //iPad 2019
            "iPad7,12"  : .iPad7,
            "iPad11,6"  : .iPad8, //iPad 2020
            "iPad11,7"  : .iPad8,
            
            //iPad Mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            "iPad11,1"  : .iPadMini5,
            "iPad11,2"  : .iPadMini5,
            
            //iPad Pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            "iPad8,1"   : .iPadPro11,
            "iPad8,2"   : .iPadPro11,
            "iPad8,3"   : .iPadPro11,
            "iPad8,4"   : .iPadPro11,
            "iPad8,9"   : .iPadPro2_11,
            "iPad8,10"  : .iPadPro2_11,
            "iPad8,5"   : .iPadPro3_12_9,
            "iPad8,6"   : .iPadPro3_12_9,
            "iPad8,7"   : .iPadPro3_12_9,
            "iPad8,8"   : .iPadPro3_12_9,
            "iPad8,11"  : .iPadPro4_12_9,
            "iPad8,12"  : .iPadPro4_12_9,
            
            //iPad Air
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad11,3"  : .iPadAir3,
            "iPad11,4"  : .iPadAir3,
            "iPad13,1"  : .iPadAir4,
            "iPad13,2"  : .iPadAir4,
            
            
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6Plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6SPlus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7Plus,
            "iPhone9,4" : .iPhone7Plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8Plus,
            "iPhone10,5" : .iPhone8Plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            "iPhone12,1" : .iPhone11,
            "iPhone12,3" : .iPhone11Pro,
            "iPhone12,5" : .iPhone11ProMax,
            "iPhone12,8" : .iPhoneSE2,
            "iPhone13,1" : .iPhone12Mini,
            "iPhone13,2" : .iPhone12,
            "iPhone13,3" : .iPhone12Pro,
            "iPhone13,4" : .iPhone12ProMax,
            
            // Apple Watch
            "Watch1,1" : .AppleWatch1,
            "Watch1,2" : .AppleWatch1,
            "Watch2,6" : .AppleWatchS1,
            "Watch2,7" : .AppleWatchS1,
            "Watch2,3" : .AppleWatchS2,
            "Watch2,4" : .AppleWatchS2,
            "Watch3,1" : .AppleWatchS3,
            "Watch3,2" : .AppleWatchS3,
            "Watch3,3" : .AppleWatchS3,
            "Watch3,4" : .AppleWatchS3,
            "Watch4,1" : .AppleWatchS4,
            "Watch4,2" : .AppleWatchS4,
            "Watch4,3" : .AppleWatchS4,
            "Watch4,4" : .AppleWatchS4,
            "Watch5,1" : .AppleWatchS5,
            "Watch5,2" : .AppleWatchS5,
            "Watch5,3" : .AppleWatchS5,
            "Watch5,4" : .AppleWatchS5,
            "Watch5,9" : .AppleWatchSE,
            "Watch5,10" : .AppleWatchSE,
            "Watch5,11" : .AppleWatchSE,
            "Watch5,12" : .AppleWatchSE,
            "Watch6,1" : .AppleWatchS6,
            "Watch6,2" : .AppleWatchS6,
            "Watch6,3" : .AppleWatchS6,
            "Watch6,4" : .AppleWatchS6,
            
            //Apple TV
            "AppleTV1,1" : .AppleTV1,
            "AppleTV2,1" : .AppleTV2,
            "AppleTV3,1" : .AppleTV3,
            "AppleTV3,2" : .AppleTV3,
            "AppleTV5,3" : .AppleTV4,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}

//extension homeViewController : GADUnifiedNativeAdLoaderDelegate {
//
//
//    // MARK: - Properties
//    /// Adds MenuItems to the tableViewItems list.
//
//    /// Add native ads to the tableViewItems list.
//
//    // MARK: - GADAdLoaderDelegate
//
//    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
//        print("\(adLoader) failed with error: \(error.localizedDescription)")
//    }
//
//    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
//        print("Received native ad: \(nativeAd)")
//        nativeAds.append(nativeAd)
//    }
//
//    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
//        self.setbelowArticlesApi(strCkeck: "get")
//    }
//
//}
