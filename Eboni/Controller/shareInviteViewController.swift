//
//  shareInviteViewController.swift
//  Eboni
//
//  Created by Apple on 11/03/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class shareInviteViewController: UIViewController {
    
    @IBOutlet weak var shareInvitePopup : shareInviteView!
    
    
    var isClick = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        self.shareInvitePopup.shareDelegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(isClick){
            isClick = false
            self.dismiss(animated: false, completion: nil)
        }
    }
}

extension shareInviteViewController : shareInvitePopupDelegate {
    
    func clickOnBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func clickOnActionBtn(strtyp: String) {
        
        self.isClick = true
        
        if(strtyp == "share"){
            
            self.dismiss(animated: true, completion: {
                let iTunesLink = "http://apple.co/3koR4mL"
                let playStoreLink = "http://bit.ly/3dQmkdc"
                let Itunes_url = URL(string: iTunesLink)!
                
                let strShare_1 = "Hayti is the #1 App for News, Videos and Podcasts from Black Publishers. \n \n\n Shared from Hayti, your all-in-one destination for news from a Black perspective.\n\n Download on iOS: \(Itunes_url) and Google Play: \(playStoreLink)"
                let shareAll = [strShare_1] as [Any]
                
                let ac = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                ac.setValue("You’re invited to download Hayti", forKey: "Subject")
                
                if let popoverController = ac.popoverPresentationController {
                        popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                        popoverController.sourceView = self.view
                        popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                }
                AppInstance.window?.rootViewController!.present(ac, animated: true, completion: nil)
            })
        }
    }
    
    
}


