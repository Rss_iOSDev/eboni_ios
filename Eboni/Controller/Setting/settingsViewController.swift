//
//  settingsViewController.swift
//  Eboni
//
//  Created by Apple on 25/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import SwiftKeychainWrapper

class settingsViewController: UIViewController {
    
    @IBOutlet weak var notLoginView : UIView!
    @IBOutlet weak var switchNotification : UISwitch!
    var setValue = "off"
    
    @IBOutlet weak var switchLight : UISwitch!
    @IBOutlet weak var switchDark : UISwitch!
    @IBOutlet weak var switchAuto : UISwitch!
    
    var setValueLight = "off"
    var setValueDark = "off"
    var setValueAuto = "off"
    
    @IBOutlet weak var lblNAme : UILabel!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var imgLogin : UIImageView!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(userGlobal?.id == nil){
            self.notLoginView.isHidden = false
        }
        else{
            self.notLoginView.isHidden = true
            
            let fname = userGlobal?.first_name
            let lname = userGlobal?.last_name
            self.lblNAme.text = "\(fname!) \(lname!)"
            
            if let socela_media = userDef.object(forKey: "socela_media") as? String {
                if(socela_media == "google"){
                    self.imgLogin.image = #imageLiteral(resourceName: "gmail")
                }
                else if(socela_media == "facebook"){
                    self.imgLogin.image = #imageLiteral(resourceName: "fb")
                }
                else if(socela_media == "apple"){
                    self.imgLogin.image = #imageLiteral(resourceName: "apple")
                }
                else if(socela_media == "email"){
                    self.imgLogin.image = #imageLiteral(resourceName: "email")
                }
            }
            self.setUpImagffromApi()
        }
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.lightMode()
            }
            else if(mode == "dark"){
                self.darkMode()
            }
            else if(mode == "auto"){
                self.AutoMode()
            }
        }
        else{
            self.AutoMode()
            
        }
        
        if let notification = userDef.object(forKey: "notification") as? String {
            self.setValue = notification
            if(notification == "on"){
                switchNotification.setOn(true, animated: false)
            }
            else{
                switchNotification.setOn(false, animated: false)
            }
        }
        else{
            switchNotification.setOn(false, animated: false)
        }
        switchNotification.addTarget(self, action: #selector(switchChanged(mySwitch:)), for: .valueChanged)
        switchLight.addTarget(self, action: #selector(switchChangeLightMode(mySwitch:)), for: .valueChanged)
        switchDark.addTarget(self, action: #selector(switchChangeDarkMode(mySwitch:)), for: .valueChanged)
        switchAuto.addTarget(self, action: #selector(switchChangeAutoMode(mySwitch:)), for: .valueChanged)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
    }
}

extension settingsViewController {

    func setUpImagffromApi(){
    
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }

        APIClient.getUerDetails(userId: userId) { (response) in
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                if let dict = response as? NSDictionary {
                    if let ThumbnailUrls = dict["ThumbnailUrls"] as? String
                    {
                        if(ThumbnailUrls != ""){
                            self.imgProfile.sd_setImage(with: URL.init(string: ThumbnailUrls), placeholderImage: UIImage(named: "unprofileIcon"))
                        }
                        else{
                            self.imgProfile.image = #imageLiteral(resourceName: "unprofileIcon")
                        }
                    }
                }
            }
        }
    
    }
}
extension settingsViewController {
    
    @IBAction func clickOnShowProfileBtn(_ sender: UIButton)
    {
        let detail = self.storyboard?.instantiateViewController(identifier: "imgUplodViewController") as! imgUplodViewController
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    @IBAction func clickOnSigninView(_ sender: UIButton)
    {
        let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
        detail.modalPresentationStyle = .overCurrentContext
        detail.vc = self
        let navC = UINavigationController(rootViewController: detail)
        navC.navigationBar.isHidden = true
        self.present(navC, animated: true, completion: nil)

    }
    
    @IBAction func clickSignOutBtn(_ sender: UIButton)
    {
        userDef.removeObject(forKey: "userInfo")
        userGlobal = nil
        userDef.removeObject(forKey: "isMode")
        userDef.removeObject(forKey: "Thumbnail")
        
       // KeychainWrapper.standard.remove(forKey: "loginapple")
        
        if let socela_media = userDef.object(forKey: "socela_media") as? String {
            if(socela_media == "google"){
                GIDSignIn.sharedInstance().signOut()
            }
            else if(socela_media == "facebook"){
                let fbLoginManager : LoginManager = LoginManager()
                fbLoginManager.logOut()
            }
            else if(socela_media == "apple"){
                
            }
        }

        
        AppInstance.window?.overrideUserInterfaceStyle = .light
        let login = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func clickSocialMediaBtn(_ sender: UIButton)
    {
        if(sender.tag == 100){
            if let url = URL(string: "https://twitter.com/haytiapp") {
                UIApplication.shared.open(url)
            }
        }
        else if(sender.tag == 200){
            if let url = URL(string: "https://www.instagram.com/haytiapp/") {
                UIApplication.shared.open(url)
            }
        }
        else if(sender.tag == 300){
            if let url = URL(string: "https://www.facebook.com/haytiapp") {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @IBAction func clickGeneralBtn(_ sender: UIButton)
    {
        if(sender.tag == 1){
            let feedback = self.storyboard?.instantiateViewController(withIdentifier: "locationViewController") as! locationViewController
            self.navigationController?.pushViewController(feedback, animated: true)
        }
        else if(sender.tag == 2){
            let feedback = self.storyboard?.instantiateViewController(withIdentifier: "aboutViewController") as! aboutViewController
            self.navigationController?.pushViewController(feedback, animated: true)
        }
        else if(sender.tag == 3){
            let feedback = self.storyboard?.instantiateViewController(withIdentifier: "feedbackViewController") as! feedbackViewController
            self.navigationController?.pushViewController(feedback, animated: true)
        }
        else if(sender.tag == 4){
            let login = self.storyboard?.instantiateViewController(withIdentifier: "privacyTermsViewController") as! privacyTermsViewController
            login.isOpenFrom = "privacy"
            self.navigationController?.pushViewController(login, animated: true)
        }
        else if(sender.tag == 5){
            let login = self.storyboard?.instantiateViewController(withIdentifier: "privacyTermsViewController") as! privacyTermsViewController
            login.isOpenFrom = "terms"
            self.navigationController?.pushViewController(login, animated: true)
        }
    }
}

extension settingsViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            userDef.removeObject(forKey: "isBack")
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.vc = self
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}

extension settingsViewController {
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        
        if(value == true){
            self.setValue = "on"
        }
        else{
            self.setValue = "off"
        }
        
        userDef.set(self.setValue, forKey: "notification")
        // Do something
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        APIClient.saveNotification(userId: userId, notification: self.setValue) { (notificationModel) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = notificationModel.Status
                if(statsu == 1){
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: notificationModel.message!)
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: notificationModel.message!)
                }
            }
        }
    }
    
    @objc func switchChangeLightMode(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        if(value == true){
            self.lightMode()
        }
        else{
            self.darkMode()
        }
    }
    
    @objc func switchChangeDarkMode(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        if(value == true){
            self.darkMode()
        }
        else{
            self.lightMode()
        }
    }
    
    @objc func switchChangeAutoMode(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        if(value == true){
            //self.darkMode()
            self.AutoMode()
        }
        else{
            if UITraitCollection.current.userInterfaceStyle == .dark {
               self.darkMode()
            }
            else {
                self.lightMode()
            }
        }
    }
    
    func darkMode(){
        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
        self.setValueDark = "on"
        self.setValueLight = "off"
        switchLight.setOn(false, animated: false)
        switchDark.setOn(true, animated: false)
        switchAuto.setOn(false, animated: false)
        AppInstance.window?.overrideUserInterfaceStyle = .dark
        userDef.set("dark", forKey: "isMode")
    }
    
    func lightMode(){
        
        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
        self.setValueLight = "on"
        self.setValueDark = "off"
        switchDark.setOn(false, animated: false)
        switchLight.setOn(true, animated: false)
        switchAuto.setOn(false, animated: false)
        AppInstance.window?.overrideUserInterfaceStyle = .light
        userDef.set("light", forKey: "isMode")
    }
    
    func AutoMode(){
        
        self.setValueDark = "off"
        self.setValueLight = "off"
        self.setValueAuto = "on"
        switchLight.setOn(false, animated: false)
        switchDark.setOn(false, animated: false)
        switchAuto.setOn(true, animated: false)
        
        userDef.set("auto", forKey: "isMode")
        
        if UITraitCollection.current.userInterfaceStyle == .dark {
           // self.darkMode()
            self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            AppInstance.window?.overrideUserInterfaceStyle = .dark
        }
        else {
           // self.lightMode()
            self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            AppInstance.window?.overrideUserInterfaceStyle = .light
        }
    }
}





extension UIImage {
    /// Fix image orientaton to protrait up
    func fixedOrientation() -> UIImage? {
        guard imageOrientation != UIImage.Orientation.up else {
            // This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }

        guard let cgImage = self.cgImage else {
            // CGImage is not available
            return nil
        }

        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil // Not able to create CGContext
        }

        var transform: CGAffineTransform = CGAffineTransform.identity

        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
        case .up, .upMirrored:
            break
        @unknown default:
            fatalError("Missing...")
            break
        }

        // Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            fatalError("Missing...")
            break
        }

        ctx.concatenate(transform)

        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }

        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}
