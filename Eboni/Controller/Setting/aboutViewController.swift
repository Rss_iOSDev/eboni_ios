//
//  aboutViewController.swift
//  Eboni
//
//  Created by Apple on 27/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import StoreKit


class aboutViewController: UIViewController {
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
    }
    
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func open(url: URL) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open \(url): \(success)")
            })
        } else if UIApplication.shared.openURL(url) {
            print("Open \(url)")
        }
    }
    
    @IBAction func clickONRateBtn(_ sender: UIButton){
        
        let str = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1544792703&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"
            
            
            //"https://apps.apple.com/app/id1544792703?action=write-review"
        
        //"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1544792703&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"
        if let checkURL = URL(string: str) {
            open(url: checkURL)
        } else {
            print("invalid url")
        }
    }
    
}

extension aboutViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
