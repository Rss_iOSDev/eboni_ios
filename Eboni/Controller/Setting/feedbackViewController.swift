
class feedbackViewController: UIViewController {
    
    @IBOutlet weak var txtVMsg: UITextView!
    @IBOutlet weak var btnFeedback: UIButton!
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.txtVMsg.placeholder = "Please leave detailed feedback"
        
        self.txtVMsg.delegate = self
        self.btnFeedback.setTitleColor(Colors.GRAY_COLOR, for: .normal)
        self.btnFeedback.isUserInteractionEnabled = false
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)


    }
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension feedbackViewController : UITextViewDelegate {
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let string1 = text
        let string2 = txtVMsg.text
        var finalString = ""
        if text.count > 0 { // if it was not delete character
            finalString = string2! + string1
            self.txtVMsg.placeholder = ""
            self.btnFeedback.setTitleColor(Colors.BLACK, for: .normal)
            self.btnFeedback.isUserInteractionEnabled = true
        }
        else if string2!.count > 0{ // if it was a delete character
            finalString = String(string2!.dropLast())
            if(finalString == ""){
                self.txtVMsg.placeholder = "Please leave detailed feedback"
                self.btnFeedback.setTitleColor(Colors.GRAY_COLOR, for: .normal)
                self.btnFeedback.isUserInteractionEnabled = false
            }
        }
        return true
        
    }
    
    
}

extension feedbackViewController {
    
    @IBAction func clickOnSendBtn(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        let sendStr = String(self.txtVMsg.text!.filter { !" \n\t\r".contains($0) })
        
        APIClient.sendFeedBack(userId: userId, msg: sendStr) { (feedBack) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = feedBack.status
                if(statsu == 1){
                    self.txtVMsg.resignFirstResponder()
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: feedBack.message!)
                    self.txtVMsg.text = ""
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: feedBack.message!)
                }
            }
        }
    }
}

extension feedbackViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)

        }
        else if(sender.tag == 30){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
