//
//  Sharing.swift
//  Eboni
//
//  Created by Apple on 20/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class MyStringItemSource: NSObject, UIActivityItemSource {
    
    
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        if activityType == UIActivity.ActivityType.mail {
            
            let full_url = userDef.value(forKey: "full_url") as? String
            let title = userDef.value(forKey: "title") as? String

            let iTunesLink = "https://apps.apple.com/app/Hayti/id1544792703"
            let playStoreLink = "http://bit.ly/3dQmkdc"
            let Itunes_url = URL(string: iTunesLink)!
            let url = URL(string: full_url!)!
            
            let strShare_1 = "\(title!) \n \(url) \n\n\n Shared from Hayti, your all-in-one destination for news from a Black perspective.\n\n Download on iOS: \(Itunes_url) and Google Play: \(playStoreLink)"
            return strShare_1
        } else {
            
            
            let nodealias = userDef.value(forKey: "nodealias") as? String
            
            let full_url = userDef.value(forKey: "full_url") as? String
            
            let compUrl = "https://www.news.hayti.com/article\(nodealias!)?fullurls=\(full_url!)"
            
            return compUrl
        }
    }
}
