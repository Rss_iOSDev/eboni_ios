//
//  locationViewController.swift
//  Eboni
//
//  Created by Apple on 27/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class locationViewController: UIViewController {
    
    @IBOutlet weak var switchLoc : UISwitch!
    @IBOutlet weak var txtZipCode : UITextField!
    
    @IBOutlet weak var btnSave : UIButton!
    var setValue = "off"
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }

        
        // Do any additional setup after loading the view.
        if let action = userDef.object(forKey: "action") as? String {
            self.setValue = action
            if(action == "on"){
                switchLoc.setOn(true, animated: false)
            }
            else{
                switchLoc.setOn(false, animated: false)
            }
        }
        else{
            switchLoc.setOn(false, animated: false)
        }
        self.btnSave.setTitleColor(Colors.GRAY_COLOR, for: .normal)
        self.btnSave.isUserInteractionEnabled = false
        switchLoc.addTarget(self, action: #selector(switchChanged(mySwitch:)), for: .valueChanged)
    }
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension locationViewController {
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        if(value == true){
            self.setValue = "on"
        }
        else{
            self.setValue = "off"
        }
        
        userDef.set(self.setValue, forKey: "action")
        // Do something
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        APIClient.saveLocationWithoutZip(userId: userId, action: self.setValue) { (locModel) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = locModel.status
                self.txtZipCode.resignFirstResponder()
                if(statsu == 1){
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: locModel.message!)
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: locModel.message!)
                }
            }
        }
    }
    
}

extension locationViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = txtZipCode.text
        var finalString = ""
        if string.count > 0 { // if it was not delete character
            finalString = string2! + string1
            self.btnSave.setTitleColor(Colors.BLACK, for: .normal)
            self.btnSave.isUserInteractionEnabled = true
        }
        else if string2!.count > 0{ // if it was a delete character
            finalString = String(string2!.dropLast())
            if(finalString == ""){
                self.btnSave.setTitleColor(Colors.GRAY_COLOR, for: .normal)
                self.btnSave.isUserInteractionEnabled = false
            }
        }
        return true
    }
    
}

extension locationViewController {
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        APIClient.saveLocationWithZipcode(userId: userId, zipCode: self.txtZipCode.text!) { (locModel) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = locModel.status
                self.txtZipCode.resignFirstResponder()
                if(statsu == 1){
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: locModel.message!)
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: locModel.message!)
                }
            }
        }
    }
}

extension locationViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)

        }
        else if(sender.tag == 30){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
