//
//  otpViewController.swift
//  Eboni
//
//  Created by Apple on 26/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class otpViewController: UIViewController {

    @IBOutlet weak var txtOtp: UITextField!
    var strEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension otpViewController {
    
    @IBAction func clickOnSendOtp(_ sender: UIButton){
        if(txtOtp.text! == "" || txtOtp.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "OTP should not left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            APIClient.showLoaderView(view: self.view)
            self.callOTPSendApi()
        }
        else{
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please check Internet connection")
        }
    }
}


extension otpViewController {
    
    func callOTPSendApi()
    {

        APIClient.verifyOtpApi(userMail: self.strEmail, userOtp: self.txtOtp.text!) { (response) in
            
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                print(response)
                
                if let resp = response as? NSDictionary{
                    
                    if let user_info = resp["user_info"] as? NSDictionary{
                        
                        if let msg = user_info["message"] as? String{
                            
                            let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                                let home = self.storyboard?.instantiateViewController(identifier: "changePassViewController") as! changePassViewController
                                home.passEmail = self.strEmail
                                self.navigationController?.pushViewController(home, animated: true)

                            }))
                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in


                            }))
                            self.present(alert, animated: true, completion: nil)

                        }
                    }
                }
            }
        }
    }
}
