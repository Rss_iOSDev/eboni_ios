//
//  createFolderViewController.swift
//  Eboni
//
//  Created by Apple on 04/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class createFolderViewController: UIViewController {
    
    @IBOutlet weak var tblFolderList: UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    
    @IBOutlet weak var btnSave : UIButton!
    
    var viewModel : folderTblListVM!
    var arrfolderList:[FolderList] = []
    var articleId = ""
    
    var viewShare: UIViewController!
    
    var openFrom = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool){
        APIClient.showLoaderView(view: self.view)
        self.callFolderListApi()
    }
    
    @IBAction func clickOnCrossBtn(_ sender: UIButton)
    {
        if(openFrom == "share"){
            self.dismiss(animated: true) {
                self.viewShare.viewWillAppear(true)
            }
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func clickOnCreateBtn(_ sender: UIButton)
    {
        let detail = self.storyboard?.instantiateViewController(identifier: "addFolderVC") as! addFolderVC
        detail.modalPresentationStyle = .overCurrentContext
        detail.viewCC = self
        detail.articleId = self.articleId
        self.present(detail, animated: true, completion: nil)
    }
}

extension createFolderViewController {
    
    func callFolderListApi(){
        
        self.arrfolderList = []
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        
        APIClient.getFolderListByUSer(userId:userId) { (folderModel) in
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                let arrData = folderModel
                for i in 0..<arrData.count{
                    let obj = arrData[i]
                    self.arrfolderList.append(obj)
                }
                self.viewModel = folderTblListVM(controller: self, items: self.arrfolderList)
                self.viewModel.vc = self
                self.viewModel.btnSave = self.btnSave
                self.tblFolderList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                self.tblFolderList.delegate = self
                self.tblFolderList.dataSource = self
                self.viewModel.tblFolder = self.tblFolderList
                if self.viewModel.items.count == 0{
                    
                }else{
                    self.tblFolderList.reloadData()
                }
            }
        }
    }
}

extension createFolderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblFolderList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblFolderList)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblFolderList)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblFolderList)
    }
}

extension createFolderViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = searchText.text
        var finalString = ""
        
        if string.isEmpty
        {
            finalString = String(string2!.dropLast())
        }
        else
        {
            finalString = string2! + string1
        }
        
        let item = viewModel.items.filter({($0.folder_name!.localizedCaseInsensitiveContains(finalString))})
        if item.count > 0
        {
            viewModel.Filtereditems = []
            viewModel.Filtereditems = item
        }
        else
        {
            viewModel.Filtereditems = []
            if(finalString == ""){
                viewModel.Filtereditems = viewModel.items
            }
        }
        self.tblFolderList.reloadData()
        return true
    }
}
