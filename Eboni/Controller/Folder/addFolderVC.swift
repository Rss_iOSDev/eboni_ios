//
//  addFolderVC.swift
//  Eboni
//
//  Created by Apple on 03/03/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class addFolderVC: UIViewController {
    
    @IBOutlet weak var viewCreateFolder: UIView!
    @IBOutlet weak var createPopup : CreateFolderPopUp!

    var viewCC : UIViewController?
    
    var articleId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        self.createPopup.createfolderDelegate = self
        self.createPopup.btnCreate.addTarget(self, action: #selector(saveBtnApi(_:)), for: .touchUpInside)

    }
    

}

extension addFolderVC : createfolderDelegate {
    
    func clickOnBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension addFolderVC {
    
    @objc func saveBtnApi(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        let sendStr = String(self.createPopup.txtfolderName.text!.filter { !" \n\t\r".contains($0) })
        APIClient.createFolder(userId: userId, folderName: sendStr, folderDesc: "") { (response) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                
                if let dict = response as? NSDictionary {
                    let statsu = dict["status"] as? Int
                    let messaage = dict["messaage"] as? String
                    if(statsu == 0){
                        APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: messaage ?? "")
                    }
                    else{
                       // self.dismiss(animated: true) {
                        self.createPopup.txtfolderName.text = ""
                        self.createPopup.txtfolderName.resignFirstResponder()
                        APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: messaage ?? "")
                        let folderId = dict["folder_id"] as? String
                        self.callSaveArticleTofolder(folderId ?? "")
                       // }
                    }
                }
            }
        }
    }
    
    func callSaveArticleTofolder(_ folderId: String)
    {
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }

        APIClient.addArticletoFolder(userId: userId, folderId: folderId, articleId: self.articleId) { (response) in
            if let dict = response as? NSDictionary {
                DispatchQueue.main.async {
                    APIClient.hideLoaderView(view: self.view)
                    let statsu = dict["status"] as! String
                    let messaage = dict["messaage"] as! String
                    if(statsu == "0"){
                        APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: messaage)
                        self.dismiss(animated: true, completion: nil)
                    }
                    else{
                        
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                // your code here
                                APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: messaage)
                            }
                        })
                        
                    }
                }
            }
        }
    }
}

extension addFolderVC {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}
