//
//  registerViewController.swift
//  Eboni
//
//  Created by Apple on 25/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase


class registerViewController: UIViewController {

    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConrfmPAss: UITextField!
    @IBOutlet weak var btnAgree: UIButton!
    
    
    var locationManager: CLLocationManager!
    var loca_curr : CLLocation?
    
    var address = ""
    var city = ""
    var country = ""
    var postalcode = ""
    var selectAgreBtn = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allowToGetLocation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
            
    }
    
    @IBAction func clickOnAgreeBtn(_ sender: UIButton)
    {
        //self.btnSave.isHidden = true
        if !self.btnAgree.isSelected {
            
            selectAgreBtn = "true"
            self.btnAgree.isSelected = true
        }
        else {
            
            selectAgreBtn = ""
            self.btnAgree.isSelected = false
        }
        
    }
    
    @IBAction func clickOnOpenPages(_ sender: UIButton)
    {
        //self.btnSave.isHidden = true
        if sender.tag == 10 {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "signPrivacyVC") as! signPrivacyVC
            login.isOpenFrom = "terms"
            self.navigationController?.pushViewController(login, animated: true)

        }
        else if sender.tag == 20{
            let login = self.storyboard?.instantiateViewController(withIdentifier: "signPrivacyVC") as! signPrivacyVC
            login.isOpenFrom = "privacy"
            self.navigationController?.pushViewController(login, animated: true)

        }
        
    }
    
}

extension registerViewController {
    
    @IBAction func clickOnContinueBtn(_ sender: UIButton){
        if(txtEmail.text! == "" || txtEmail.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Email address should not left blank.")
        }
        else if(!(txtEmail.text!.isValidEmail())){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Email address is not valid.")
        }
        else if(txtFirstName.text! == "" || txtFirstName.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Name should not left blank.")
        }
        else if(txtPassword.text! == "" || txtPassword.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Password should not left blank.")
        }
        else if(txtConrfmPAss.text! == "" || txtConrfmPAss.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please enter password again.")
        }
        else if(txtPassword.text! != txtConrfmPAss.text){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Password do not match.")
        }
        else if(self.selectAgreBtn == ""){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please Agree to Hayti's Terms nd Conditions.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            APIClient.showLoaderView(view: self.view)
            self.callRegisterUSerApi()
        }
        else{
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please check Internet connection")
        }
    }
}

extension registerViewController :CLLocationManagerDelegate{
    
    func allowToGetLocation()
    {
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        self.loca_curr = location
        self.getAddressFromLatLong(expLoc: self.loca_curr!)
        
    }
    
    func getAddressFromLatLong(expLoc: CLLocation){
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(expLoc, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            
            if(placemarks != nil){
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    // Location name
                    if pm.name != nil {
                        self.address = pm.name!
                    }
                    
                    if pm.locality != nil{
                        self.city = pm.locality!
                    }
                    
                    // Street address
                    if pm.postalCode != nil {
                        self.postalcode = pm.postalCode!
                    }
                    
                    // Country
                    if pm.country != nil {
                        self.country =  pm.country!
                    }
              }
            }
        })
    }
}

extension registerViewController {
    
    func callRegisterUSerApi()
    {
            Analytics.logEvent("signup_event", parameters: [
                "type": "email"
            ])
            
            var tokan = ""
            if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
                tokan = fcmToken
            }
            
            var deviceID = ""
            if let device_token = userDef.object(forKey: "device_token") as? String{
                deviceID = device_token
            }
        
        APIClient.callLoginapiWithemail(email: self.txtEmail.text!, address: self.address, city: city, country: country, postalcode: postalcode, profile: "", appname: Endpoints.Environment.appName, regsource: "email", tokens: tokan, device_id: deviceID, action: "signup", setpassword: self.txtPassword.text!,strTyp: "register", firstName: self.txtFirstName.text!) { (loginModel) in
            
                OperationQueue.main.addOperation {
                    let encoder = JSONEncoder()
                    
                    let strMsg = loginModel.user_info?.message
                    if(strMsg == "user already registered."){
                        
                        APIClient.hideLoaderView(view: self.view)
                        let home = self.storyboard?.instantiateViewController(identifier: "userExistVC") as! userExistVC
                        self.navigationController?.pushViewController(home, animated: true)
                    }
                    else{
                        
                        
                        
                        if let encoded = try? encoder.encode(loginModel.user_info!) {
                            userDef.set(encoded, forKey: "userInfo")
                            userDef.set("email", forKey: "socela_media")
                        }
                        
                        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
                            let decoder = JSONDecoder()
                            if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                                userGlobal = userM
                                
                                APIClient.hideLoaderView(view: self.view)
                                
                                let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                                self.navigationController?.pushViewController(home, animated: true)
                            }
                        }
                    }
                    

                }
            }
    }
}


extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)

    }

}
