//
//  publisherListViewController.swift
//  Eboni
//
//  Created by Apple on 17/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

class publisherListViewController: UIViewController {
    
    var publishrId : String?
    
    var publishArticleList:[AnyObject] = []
    var viewModel : articleListVM!
    
    @IBOutlet weak var tblPublishArticleList : UITableView!
    @IBOutlet weak var lblTitle : UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var addsPIndex = 5
    
    var scrollIndex = 1
    var totalPageCount: Int?
    
    var refreshTbl: UIRefreshControl!
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var hgtFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    var isFetching = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                        
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        refreshTbl = UIRefreshControl()
        refreshTbl.addTarget(self, action: #selector(onRefreshTbl), for: .valueChanged)
        tblPublishArticleList.addSubview(refreshTbl)
        
        self.viewFooter.isHidden = true
        self.hgtFooter.constant = 0
        self.spinner.isHidden = true
        
        self.view.bringSubviewToFront(viewFooter)
        
        self.loadBanner(self.bannerView)
        
        
        
        
        APIClient.showLoaderView(view: self.view)
        self.setPublishArticlesApi(strCkeck: "get")
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension publisherListViewController {
    
    func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    @objc func onRefreshTbl() {
        run(after: 2) {
            self.scrollIndex = 1
            self.publishArticleList = []
            APIClient.showLoaderView(view: self.view)
            self.setPublishArticlesApi(strCkeck: "get")
            self.refreshTbl.endRefreshing()
        }
    }
}

extension publisherListViewController {
    
    func setPublishArticlesApi(strCkeck: String){
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.publishArticleList = []
        }
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        APIClient.belowArticlebyPublisherId(userId: userId, pageNo: "\(self.scrollIndex)", publisher_id: self.publishrId!) { (belowModel) in
            
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                
                if let dict = belowModel as? NSDictionary{
                    let total_no_of_page = dict["total_no_of_page"] as? Int
                    if let arrData = dict["allarticlesLists"] as? [NSDictionary]{
                        
                        if(strCkeck == "get"){
                            
                            if(arrData.count == 0){
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                            else{
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.publishArticleList.append(obj)
                                }
                                
                                for i in 0..<self.publishArticleList.count{
                                        if(i == self.addsPIndex){
                                            self.publishArticleList.insert("Insert_banner" as AnyObject, at: self.addsPIndex)
                                            self.addsPIndex += 6
                                        }
                                }

                                
                                

                                
                                self.totalPageCount = total_no_of_page
                                self.viewModel = articleListVM(controller: self, items: self.publishArticleList)
                                self.viewModel.viewContrlr = self
                                self.viewModel.viewContoller = self
                                self.viewModel.typeVC = "publish"
                                self.tblPublishArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                
                                
                                
                                self.tblPublishArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                    forCellReuseIdentifier: "bannerAdsCell")
                                
                                self.tblPublishArticleList.delegate = self
                                self.tblPublishArticleList.dataSource = self
                                
                                let obj = self.publishArticleList[0] as! NSDictionary
                                self.lblTitle.text = obj["field_web_name_value"] as? String
                                
                                self.isFetching = false
                                self.tblPublishArticleList.reloadData()
                            }
                        }
                        else{
                            if (arrData.count > 0)
                            {
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.publishArticleList.append(obj)
                                }

                                for i in 0..<self.publishArticleList.count{
                                        if(i == self.addsPIndex){
                                            self.publishArticleList.insert("Insert_banner" as AnyObject, at: self.addsPIndex)
                                            self.addsPIndex += 6
                                        }
                                }

                                
                                self.totalPageCount = total_no_of_page
                                self.viewModel = articleListVM(controller: self, items: self.publishArticleList)
                                self.viewModel.viewContrlr = self
                                self.viewModel.viewContoller = self
                                self.viewModel.typeVC = "publish"
                                self.tblPublishArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                
                                self.tblPublishArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                                    forCellReuseIdentifier: "bannerAdsCell")
                                
                                
                                self.tblPublishArticleList.delegate = self
                                self.tblPublishArticleList.dataSource = self

                                
                                let obj = self.publishArticleList[0] as! NSDictionary
                                self.lblTitle.text = obj["field_web_name_value"] as? String
                                
                                self.isFetching = false
                                self.tblPublishArticleList.reloadData()
                                
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                                
                            }
                            else{
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                        }
                    }
                }
            }
        }
    }
}

extension publisherListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblPublishArticleList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblPublishArticleList)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblPublishArticleList)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblPublishArticleList)
    }
    
}

extension publisherListViewController {
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        
    }
}
