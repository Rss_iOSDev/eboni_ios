//
//  loginByEmailVC.swift
//  Eboni
//
//  Created by Apple on 25/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase



//protocol afterLoginReloadDelegate{
//    
//    func reloadDataonPage()
//}



class loginByEmailVC: UIViewController {
    
    var isViewOpenFrom = ""
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var locationManager: CLLocationManager!
    var loca_curr : CLLocation?
    
    var address = ""
    var city = ""
    var country = ""
    var postalcode = ""

    var popupvc: loginPopupVC!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allowToGetLocation()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)


        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        //if(self.isViewOpenFrom == "page_1"){
            self.navigationController?.popViewController(animated: true)
            
//        }else if(self.isViewOpenFrom == "page_2"){
//            self.dismiss(animated: true) {
//            }
//        }
    }
    
    @IBAction func clickOnSignup(_ sender: UIButton)
    {
        let register = self.storyboard?.instantiateViewController(identifier: "registerViewController") as! registerViewController
        self.navigationController?.pushViewController(register, animated: true)

    }
    
    @IBAction func clickOnForgetPAss(_ sender: UIButton)
    {
        let register = self.storyboard?.instantiateViewController(identifier: "forgetPassViewContoller") as! forgetPassViewContoller
        self.navigationController?.pushViewController(register, animated: true)

    }

}

extension loginByEmailVC {
    
    @IBAction func clickOnLogin(_ sender: UIButton){
        if(txtEmail.text! == "" || txtEmail.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Email address should not left blank.")
        }
        else if(txtPassword.text! == "" || txtPassword.text!.count == 0){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Password should not left blank.")
        }
        else if(!(txtEmail.text!.isValidEmail())){
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Email address is not valid.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            APIClient.showLoaderView(view: self.view)
            self.callLoginApi()
        }
        else{
            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Please check Internet connection")
        }
    }
}

extension loginByEmailVC :CLLocationManagerDelegate{
    
    func allowToGetLocation()
    {
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        self.loca_curr = location
        self.getAddressFromLatLong(expLoc: self.loca_curr!)
        
    }
    
    func getAddressFromLatLong(expLoc: CLLocation){
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(expLoc, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            
            if(placemarks != nil){
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    // Location name
                    if pm.name != nil {
                        self.address = pm.name!
                    }
                    
                    if pm.locality != nil{
                        self.city = pm.locality!
                    }
                    
                    // Street address
                    if pm.postalCode != nil {
                        self.postalcode = pm.postalCode!
                    }
                    
                    // Country
                    if pm.country != nil {
                        self.country =  pm.country!
                    }
              }
            }
        })
    }
}

extension loginByEmailVC {
    
    func callLoginApi()
    {
            Analytics.logEvent("login_event", parameters: [
                "type": "email"
            ])
            
            var tokan = ""
            if let fcmToken = userDef.object(forKey: "fcmToken") as? String{
                tokan = fcmToken
            }
            
            var deviceID = ""
            if let device_token = userDef.object(forKey: "device_token") as? String{
                deviceID = device_token
            }
        
        
        
        APIClient.callLoginapiWithemail(email: self.txtEmail.text!, address: self.address, city: city, country: country, postalcode: postalcode, profile: "", appname: Endpoints.Environment.appName, regsource: "email", tokens: tokan, device_id: deviceID, action: "login", setpassword: self.txtPassword.text!, strTyp: "", firstName: "") { (loginModel) in
            
            print(loginModel.user_info?.message)
        
            
                OperationQueue.main.addOperation {
                    
                    APIClient.hideLoaderView(view: self.view)
                    
                    let strMsg = loginModel.user_info?.message
                    
                    if(strMsg == "You are successfully logged in."){
                        let encoder = JSONEncoder()
                        if let encoded = try? encoder.encode(loginModel.user_info!) {
                            userDef.set(encoded, forKey: "userInfo")
                            userDef.set("email", forKey: "socela_media")
                        }
                        
                        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
                            let decoder = JSONDecoder()
                            if let userM = try? decoder.decode(User_info.self, from: userInfo) {
                                userGlobal = userM
                                
                                if(self.isViewOpenFrom == "page_1"){
                                    let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                                    self.navigationController?.pushViewController(home, animated: true)
                                    
                                }else if(self.isViewOpenFrom == "page_2"){
                                    self.dismiss(animated: true) {
                                        self.popupvc.clickOnBack()
                                    }
                                }
                            }
                        }
                    }
                    else{
                        
                        OperationQueue.main.addOperation {
                            APIClient.hideLoaderView(view: self.view)
                            APIClient.showAlertMessage(vc: self, titleStr: "Alert", messageStr: loginModel.user_info?.message ?? "Some error occured")

                        }
                    }
                }
            }
    }
}
