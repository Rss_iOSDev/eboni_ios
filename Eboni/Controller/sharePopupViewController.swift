//
//  sharePopupViewController.swift
//  Eboni
//
//  Created by Apple on 15/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class sharePopupViewController: UIViewController {
    
    @IBOutlet weak var shareFolderPopup : sharePopUpView!
    
    var strLink = ""
    var nid = ""
    var strTitle = ""
    var nodeAlias = ""
    
    var isClick = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shareFolderPopup.shareDelegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(isClick){
            isClick = false
            self.dismiss(animated: false, completion: nil)
        }
    }
}

extension sharePopupViewController : sharePopupDelegate , UIActivityItemSource{
    
    func clickOnBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func clickOnActionBtn(strtyp: String) {
        
        self.isClick = true
        if(strtyp == "add"){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
            }
            else{
                let detail = self.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
                detail.modalPresentationStyle = .overCurrentContext
                detail.articleId = self.nid
                detail.viewShare = self
                detail.openFrom = "share"
                self.present(detail, animated: true, completion: nil)
            }
        }
        else if(strtyp == "share"){
            
            self.dismiss(animated: true, completion: {
                
                let items = [self]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                if let popoverController = ac.popoverPresentationController {
                        popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                        popoverController.sourceView = self.view
                        popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                }
                AppInstance.window?.rootViewController!.present(ac, animated: true, completion: nil)
            })
        }
        else if(strtyp == "copy"){
            self.dismiss(animated: true){
                let pasteboard = UIPasteboard.general
                pasteboard.string = self.strLink
                self.strLink = ""
                self.strLink = pasteboard.string!
            }
        }
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }

    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        if activityType == UIActivity.ActivityType.mail{
            let iTunesLink = "https://apps.apple.com/app/Hayti/id1544792703"
            let playStoreLink = "http://bit.ly/3dQmkdc"
            let Itunes_url = URL(string: iTunesLink)!
            let url = "https://www.hayti.com\(self.nodeAlias)"
            let strShare_1 = "\(self.strTitle) \n \(url) \n\n\n Shared from Hayti, your all-in-one destination for news from a Black perspective.\n\n Download on iOS: \(Itunes_url) and Google Play: \(playStoreLink)"
            return strShare_1
        }
        else {
            let compUrl = "https://www.hayti.com\(self.nodeAlias)"
            return compUrl
        }
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return self.strTitle
    }
}

//https://www.hayti.com/news
