

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import Firebase
import GoogleMobileAds
import FirebaseMessaging
import SwiftyJSON
import UserNotificationsUI
import UXCam
import AppTrackingTransparency
import Reachability
import IQKeyboardManagerSwift


let reach = try? Reachability()
var userDef = UserDefaults.standard
var userGlobal : User_info?

var AppInstance: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var notificationObject = [String:AnyObject]()
    
    var window:UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppInstance = self
        GIDSignIn.sharedInstance().clientID = Endpoints.Environment.googleClientId
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        self.callRootView()
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        UXCam.optIntoSchematicRecordings()
        UXCam.start(withKey:"j836xi9kbjvvdug")
        
        
        
        
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [ "2077ef9a63d2b398840261c8221a0c9b" ]
        
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { (status) in
                switch status {
                case .authorized:
                    print("Yeah, we got permission :)")
                case .denied:
                    print("Oh no, we didn't get the permission :(")
                case .notDetermined:
                    print("Hmm, the user has not yet received an authorization request")
                case .restricted:
                    print("Hmm, the permissions we got are restricted")
                @unknown default:
                    print("Looks like we didn't get permission")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        
        if let notificationResponse = launchOptions?[.remoteNotification] as? UNNotificationResponse{
            
            let userInfo = notificationResponse.notification.request.content.userInfo
            
            let jsonY = JSON(userInfo)
            print("json-----\(jsonY)")
            
            let notification = jsonY["notification"].stringValue
            print(notification)
            
            //  var notificationDict = [String: AnyObject]()
            if let data = notification.data(using: String.Encoding.utf8) {
                do {
                    let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                    self.notificationObject = dictonary!
                    
                    NotificationCenter.default.post(name: .articleForNotification, object: self.notificationObject)
                    
                    let state = UIApplication.shared.applicationState
                    
                    if state ==  .inactive {
                        
                        self.notificationObject = dictonary!
                        let title = dictonary?["publisher_name"] as! String
                        let body = dictonary?["article_name"] as! String
                        self.callLocalNotification(title: title, body: body)
                        NotificationCenter.default.post(name: .addNotification, object: self.notificationObject)
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            }
        }
        self.registerForPushNotifications()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if (url.scheme == "fb1223322811369147")
        {
            return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.setMode()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.setMode()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.setMode()
    }
}

extension AppDelegate {
    
    func callRootView(){
        
        self.setMode()
        let _window = UIWindow(frame: UIScreen.main.bounds)
        window = _window
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "splashViewController") as! splashViewController
        let rootViewController = initialViewController
        let navigationController = UINavigationController()
        navigationController.navigationBar.isHidden = true
        navigationController.viewControllers = [rootViewController]
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
    }
}

extension AppDelegate  : UNUserNotificationCenterDelegate, MessagingDelegate{
    
    
    func registerForPushNotifications() {
        
        
        Messaging.messaging().delegate = self
        
        //        if #available(iOS 10.0, *) {
        //            // For iOS 10 display notification (sent via APNS)
        //            UNUserNotificationCenter.current().delegate = self
        //            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        //            UNUserNotificationCenter.current().requestAuthorization(
        //                options: authOptions,
        //                completionHandler: {_, _ in
        //                    // For iOS 10 data message (sent via FCM)
        //                    DispatchQueue.main.async {
        //                        UIApplication.shared.registerForRemoteNotifications()
        //                    }
        //
        //                    let action1 = UNNotificationAction(identifier: "actionTap", title: "Hayti", options: [.foreground])
        //                    let category = UNNotificationCategory(identifier: "CategoryIdentifier", actions: [action1], intentIdentifiers: [], options: [])
        //                    UNUserNotificationCenter.current().setNotificationCategories([category])
        //
        //                })
        //
        //        } else {
        //            let settings: UIUserNotificationSettings =
        //                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        //            UIApplication.shared.registerUserNotificationSettings(settings)
        //        }
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        
        self.updateFirestorePushTokenIfNeeded()
        // NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)
        
    }
    
    
    func updateFirestorePushTokenIfNeeded() {
        // if let token = Messaging.messaging().fcmToken {
        
        let instance = InstanceID.instanceID()
        //        instance.deleteID { (error) in
        //          print(error.debugDescription)
        //        }
        
        instance.instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else {
                print("Remote instance ID token: \(String(describing: result?.token))")
                
                let strToken = result?.token
                userDef.setValue(strToken!, forKey: "fcmToken")
                
                //            if let device_token = userDef.object(forKey: "device_token") as? String{
                //                APIClient.sendRefeshToken(device_id: device_token, refreshTokens: strToken!) { (response) in
                //                    print(response)
                //                }
                //            }
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        userDef.setValue(deviceTokenString, forKey: "device_token")
        userDef.synchronize()
    }
    
    
    //    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    //        print(remoteMessage.appData)
    //    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        userDef.setValue(fcmToken, forKey: "fcmToken")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // let userInfo = notification.request.content.userInfo
        completionHandler([.alert,.sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        switch response.actionIdentifier {
        
        case UNNotificationDefaultActionIdentifier:
            
            
            
            //            let jsonY = JSON(userInfo)
            //            print("json-----\(jsonY)")
            //
            //            let notification = jsonY["notification"].stringValue
            //            print(notification)
            //
            //            var notificationDict = [String: AnyObject]()
            //            if let data = notification.data(using: String.Encoding.utf8) {
            //                do {
            //                    let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            //                    notificationDict = dictonary!
            //                    NotificationCenter.default.post(name: .articleForNotification, object: notificationDict)
            //                } catch let error as NSError {
            //                    print(error)
            //                }
            //            }
            
            print(self.notificationObject)
            NotificationCenter.default.post(name: .articleForNotification, object: self.notificationObject)
            
            completionHandler()
        default:
            break;
            
        }
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
        let jsonY = JSON(userInfo)
        print("json-----\(jsonY)")
        
        let notification = jsonY["notification"].stringValue
        print(notification)
        
        if let data = notification.data(using: String.Encoding.utf8) {
            do {
                let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                self.notificationObject = dictonary!
                let title = dictonary?["publisher_name"] as! String
                let body = dictonary?["article_name"] as! String
                self.callLocalNotification(title: title, body: body)
                NotificationCenter.default.post(name: .addNotification, object: self.notificationObject)
            } catch let error as NSError {
                print(error)
            }
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func callLocalNotification(title:String , body : String ){
        if #available(iOS 10.0, *){
            //UNUserNotificationCenter.current().delegate = self
            let content = UNMutableNotificationContent()
            let requestIdentifier = "actionTap"
            content.categoryIdentifier = "CategoryIdentifier"
            content.sound = UNNotificationSound.default
            content.title = title
            content.body = body
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
            let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error:Error?) in
                if error != nil{
                    print(error?.localizedDescription ?? 0)
                }
                print("Notification Register Success")
            }
        }
        
    }
}


extension Notification.Name {
    static let articleForNotification = Notification.Name("articleForNotification")
    static let didCloseSession = Notification.Name("didCloseSession")
    static let addNotification = Notification.Name("addNotification")
    static let deepLinking = Notification.Name("deepLinking")
}


extension AppDelegate {
    
    func setMode(){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                window?.overrideUserInterfaceStyle = .light
            }
            else if(mode == "dark"){
                window?.overrideUserInterfaceStyle = .dark
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        window?.overrideUserInterfaceStyle = .dark
                    }
                    else {
                        window?.overrideUserInterfaceStyle = .light
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                userDef.set("auto", forKey: "isMode")
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    window?.overrideUserInterfaceStyle = .dark
                }
                else {
                    window?.overrideUserInterfaceStyle = .light
                }
            }
        }
    }
    
}


extension AppDelegate {
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL {
            var view = url.lastPathComponent
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
            }
            
            // redirect(to: view, with: parameters)
        }
        return true
    }
}


extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}




//    "data" : "{\"thumbnail\":\"https:\\\/\\\/d1gikeh9vp02a9.cloudfront.net\\\/publisher_image\\\/pub12281437.png\",\"publisher_name\":\"BET\",\"full_url\":\"https:\\\/\\\/www.bet.com\\\/style\\\/fashion\\\/photos\\\/reginae-carter-fashion-moments.html\",\"sound\":\"default\",\"nid\":\"366792\",\"title\":\"Toya Johnson Poses With Her Daughters For A Magazine!\",\"total_likes\":\"0\",\"isFavorite\":\"0\"}",
//    "notification" : "{\"thumbnail\":\"https:\\\/\\\/d1gikeh9vp02a9.cloudfront.net\\\/publisher_image\\\/pub12281437.png\",\"publisher_name\":\"BET\",\"full_url\":\"https:\\\/\\\/www.bet.com\\\/style\\\/fashion\\\/photos\\\/reginae-carter-fashion-moments.html\",\"sound\":\"default\",\"nid\":\"366792\",\"title\":\"Toya Johnson Poses With Her Daughters For A Magazine!\",\"total_likes\":\"0\",\"isFavorite\":\"0\"}",
//    "gcm.notification.thumbnail" : "https:\/\/d1gikeh9vp02a9.cloudfront.net\/publisher_image\/pub12281437.png",
//    "gcm.notification.full_url" : "https:\/\/www.bet.com\/style\/fashion\/photos\/reginae-carter-fashion-moments.html",
//    "gcm.notification.publisher_name" : "BET",
//    "gcm.notification.isFavorite" : "0",
//    "google.c.a.e" : "1",
//    "gcm.notification.total_likes" : "0",
//    "aps" : {
//      "content-available" : "1",
//      "mutable-content" : "1",
//      "alert" : {
//        "title" : "Toya Johnson Poses With Her Daughters For A Magazine!"
//      },
//      "sound" : "default"
//    },
//    "gcm.notification.nid" : "366792",
//    "google.c.sender.id" : "679844424322",
//    "gcm.message_id" : "1620657929908869"
//  }


//<dict>
//    <key>CFBundleURLName</key>
//    <string>com.Eboni.ios</string>
//    <key>CFBundleURLSchemes</key>
//    <array>
//        <string>applinks</string>
//    </array>
//</dict>

extension AppDelegate {
    
    func application(_: UIApplication, continue userActivity: NSUserActivity, restorationHandler _: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL else {
            return false
        }
        print(url)
        
        if let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true){
            // let params = components.queryItems {
            let pth = components.path
            let strngArr = pth?.components(separatedBy: ".com")
            
            let nmAlias = strngArr?[0]
            
            var userId = ""
            if(userGlobal?.id == nil){
                userId = "0"
            }
            else{
                userId = (userGlobal?.id)!
            }
            
            APIClient.getDetailArticleFromLink(aliaspath: nmAlias!, userid: userId) { (response) in
                if let resp = response as? NSDictionary{
                    if let node_info = resp["node_info"] as? NSDictionary {
                        NotificationCenter.default.post(name: .deepLinking, object: node_info)
                    }
                }
            }
        }
        return true
    }
}
