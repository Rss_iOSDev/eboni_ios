
import Foundation

struct Endpoints {
    
    struct Environment {
        
        static let baseEnvironment = "https://www.hayti.com/"    //"https://www.eboniapp.com/"
        static let webapi = "webapi"
        static let baseURL =  baseEnvironment + webapi
        static let appName =  "IOS"
        static let googleClientId = "679844424322-3pbf9fq1unir2ebe2hoiovgf7gcf883m.apps.googleusercontent.com"
        
        static let googleAdsHomeUniId = "ca-app-pub-6251292548311563/3704349594"
        
        static let googleAdsBannerCell = "ca-app-pub-6251292548311563/2354375370"
            
            //"ca-app-pub-3940256099942544/3986624511" native banner
        
        static let bannerUniId = "ca-app-pub-6251292548311563/7751421727"
        
        //"ca-app-pub-3940256099942544/2934735716" test banner
        
      //  static let googleAdsCatUniId = "ca-app-pub-6251292548311563/6847719387"

    }
    struct User {
        static let login = "login"
        static let usersrefreshtokens = "usersrefreshtokens"
        static let email_signup_login = "email-signup-login"
        static let forgot_password = "forgot-password"
        
        
        
        
    }
    
    struct Category {
        static let categorylist = "categorylist"
        static let todayblackhistory = "todayblackhistory"
        static let homepageboxmessage = "homepage-box-message"
        static let articles_top10 = "articles-top10"
        static let below_top10 = "below-top10"
        static let articlelistbycategory = "articlelistbycategory"
        static let articlelistbypublisher = "articlelistbypublisher"
        static let searcharticles = "searcharticles"
        static let articleviews = "articleviews"
        static let likearticles = "likearticles"
        static let article_detail_alias = "article-detail-alias"
        
        
    }
    
    struct Following {
        static let user_cat_channel_list = "user-cat-channel-list"
        static let followingitems = "followingitems"
        static let user_following = "user-following"
        static let categorylistFollowing = "categorylist"
        static let channellist = "channellist"
        static let channellist_search = "channellist-search"
    }
    
    
    struct Setting {
        static let privacy_policy = "privacy-policy"
        static let terms_of_service = "terms-of-service"
        static let user_feedback = "user-feedback"
        static let user_location = "user-location"
        static let user_notification = "user-notification"
        static let user_tumbnail = "user-tumbnail"
        static let user_details = "user-details"
    }
    
    struct Folder {
        static let user_allfolderlist = "user-allfolderlist"
        static let user_folderlist = "user-folderlist"
        static let user_createfolder = "user-createfolder"
        static let user_folder_operation = "user-folder-operation"
        static let user_articlelistbyfolder = "user-articlelistbyfolder"
        static let user_articledelete_from_folder = "user-articledelete-from-folder"
        static let user_additemtofolder = "user-additemtofolder"
    }
    
//    https://www.eboniapp.com/webapi-user-details/?userid=1
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case connection = "keep-alive"
    case contentLenght = "Content-Lenght"
}

enum ContentType: String {
    case json = "application/json"
}

//old gmail client id - "156156470068-td6g9rudj5s9snqj989206g0fs89pv1q.apps.googleusercontent.com"
