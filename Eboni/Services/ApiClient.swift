//
//  ApiClient.swift
//  Eboni
//
//  Created by Apple on 23/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//


import Alamofire
import Foundation
import SwiftyJSON

class APIClient {
    
    static let window = (UIApplication.shared.delegate as! AppDelegate).window
    
    
    static func showLoaderView(view: UIView){
        
        customLoader.sharedInstance.startAnimation()
        
        //        let spinnerActivity = MBProgressHUD.showAdded(to: view, animated: true);
        //        spinnerActivity.mode = .indeterminate
        //        spinnerActivity.contentColor = Colors.THEME_COLOR
        //        spinnerActivity.isUserInteractionEnabled = false
    }
    
    static func hideLoaderView(view: UIView){
        //  MBProgressHUD.hide(for: view, animated: true)
        
        customLoader.sharedInstance.stopAnimation()
    }
    
    static func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: { action in
        }))
        vc.present(alert, animated: true, completion: nil)
        
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                //Globalfunc.print(object:error.localizedDescription)
            }
        }
        return nil
    }
    
    static func showNotificationAlert(dict:JSON){
        
        let title = dict["alert"]["title"].stringValue
        
        var originY:CGFloat = 0.0
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.bounds.height >= 812 {
            originY = 20
        }
        let errorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width , height: 0))
        errorView.backgroundColor = UIColor.white
        errorView.clipsToBounds = false
        errorView.layer.masksToBounds = false
        errorView.layer.shadowOffset = CGSize(width: 0, height: 1)
        errorView.layer.shadowOpacity = 0.2
        errorView.layer.shadowRadius = 1
        errorView.layer.cornerRadius = 2
        let lb_error = UILabel(frame: CGRect(x:  UIScreen.main.bounds.width * 0.23, y: 0, width: UIScreen.main.bounds.width * 0.78, height: 0))
        lb_error.numberOfLines = 0
        lb_error.textAlignment = .left
        lb_error.textColor = .black
        lb_error.font = UIFont.systemFont(ofSize: 14)
        lb_error.text = title
        
        let noNet_ico = UIImageView(frame: CGRect(x: 0, y: -35, width: UIScreen.main.bounds.width * 0.23, height: 70))
        //  noNet_ico.image = UIImage(named: icon)
        noNet_ico.contentMode = .center
        
        errorView.addSubview(lb_error)
        //  UtilesSwift.shared.getTopViewController().view.addSubview(errorView)
        errorView.addSubview(noNet_ico)
        UIView.animate(withDuration: 0.4, animations: {
            errorView.frame = CGRect(x: 0, y: originY, width: UIScreen.main.bounds.width, height: 80)
            lb_error.frame = CGRect(x:  UIScreen.main.bounds.width * 0.23, y: 20, width: UIScreen.main.bounds.width * 0.78, height: 60)
            noNet_ico.frame = CGRect(x: 0, y: 15, width: UIScreen.main.bounds.width * 0.23, height: 70)
        }, completion: {
            finished in
            UIView.animate(withDuration: 0.3, delay: 2.5, options: .curveEaseIn, animations: {
                errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0)
                lb_error.frame = CGRect(x:  UIScreen.main.bounds.width * 0.23, y: 0, width: UIScreen.main.bounds.width * 0.78, height: 0)
                noNet_ico.frame = CGRect(x: 0, y: -35, width:  UIScreen.main.bounds.width * 0.23, height: 70)
            }, completion: {finished in
                errorView.removeFromSuperview()
            })
        })
    }
    
    static func login(email:String, firstName:String,lastName:String,address:String,city:String,country:String,postalcode:String,profile: String,appname:String,regsource:String,tokens: String, completion:@escaping (loginModel)->Void){
        
        let address_Space = address.replacingOccurrences(of: " ", with: "%20")
        let country_Space = country.replacingOccurrences(of: " ", with: "%20")
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.login)?userMail=\(email)&firstName=\(firstName)&lastName=\(lastName)&address=\(address_Space)&city=\(city)&country=\(country_Space)&postalcode=\(postalcode)&profilethumburl=\(profile)&appname=\(appname)&regsource=\(regsource)&tokans=\(tokens)"
        
        print(url)
        
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(loginModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func callLoginapi(email:String, firstName:String,lastName:String,address:String,city:String,country:String,postalcode:String,profile: String,appname:String,regsource:String,tokens: String, device_id: String, completion:@escaping (loginModel)->Void){
        
        let address_Space = address.replacingOccurrences(of: " ", with: "%20")
        let country_Space = country.replacingOccurrences(of: " ", with: "%20")
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.login)?userMail=\(email)&firstName=\(firstName)&lastName=\(lastName)&address=\(address_Space)&city=\(city)&country=\(country_Space)&postalcode=\(postalcode)&profilethumburl=\(profile)&appname=\(appname)&regsource=\(regsource)&tokans=\(tokens)&device_id=\(device_id)"
        
        print(url)
        
        
        var searchURL = NSURL()
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        }
        else {
            let Nurl : NSString = url as NSString
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
        }
        
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if let httpStatus = response as? HTTPURLResponse,
               
               httpStatus.statusCode != 200 { // check for httperrors
                if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                {
                    print(parsedData)
                }
            }
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                    //let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    // completionHandler(json, "")
                    
                    let userResponse = try JSONDecoder().decode(loginModel.self, from: data!)
                    completion(userResponse)
                    
                    //do your stuff
                    
                } catch {
                    let json : Any = (Any).self
                    //  completionHandler(json, "Status_Not_200")
                }
            }
            else if error != nil
            {
                let json : Any = (Any).self
                //  completion(json)
            }
        }).resume()
        
        
        
    }
    
    


    static func callLoginapiWithemail(email:String,address:String,city:String,country:String,postalcode:String,profile: String,appname:String,regsource:String,tokens: String, device_id: String, action:String,  setpassword:String, strTyp: String, firstName:String, completion:@escaping (loginModel)->Void){
        
        let address_Space = address.replacingOccurrences(of: " ", with: "%20")
        let country_Space = country.replacingOccurrences(of: " ", with: "%20")
        
        var url = ""
        
        if(strTyp == ""){
            url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.email_signup_login)?userMail=\(email)&address=\(address_Space)&city=\(city)&country=\(country_Space)&postalcode=\(postalcode)&profilethumburl=\(profile)&appname=\(appname)&regsource=\(regsource)&tokans=\(tokens)&device_id=\(device_id)&action=\(action)&setpassword=\(setpassword)"
        }
        else{
            url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.email_signup_login)?userMail=\(email)&address=\(address_Space)&city=\(city)&country=\(country_Space)&postalcode=\(postalcode)&profilethumburl=\(profile)&appname=\(appname)&regsource=\(regsource)&tokans=\(tokens)&device_id=\(device_id)&action=\(action)&setpassword=\(setpassword)&firstName=\(firstName)"
        }
        
        print(url)
        
        
        var searchURL = NSURL()
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        }
        else {
            let Nurl : NSString = url as NSString
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
        }
        
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if let httpStatus = response as? HTTPURLResponse,
               
               httpStatus.statusCode != 200 { // check for httperrors
                if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                {
                    print(parsedData)
                }
            }
            if error == nil && data != nil {
                do {
                    
                    let userResponse = try JSONDecoder().decode(loginModel.self, from: data!)
                    completion(userResponse)
                    
                    //do your stuff
                    
                } catch {
                    let json : Any = (Any).self
                    //  completionHandler(json, "Status_Not_200")
                }
            }
            else if error != nil
            {
                let json : Any = (Any).self
                //  completion(json)
            }
        }).resume()
        
        
        
    }
    
    
    static func categoryList(userId:String, completion:@escaping ([Category_info])->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.categorylist)?featured=yes&userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let rr = response.result.value as! NSDictionary
                    let arrCat = rr["category_info"] as! [NSDictionary]
                    
                    /*                    let newDict : NSMutableDictionary = [:]
                     newDict.setValue("My News", forKey: "cat_name")
                     newDict.setValue("1", forKey: "cat_id")
                     arrCat.insert(newDict, at: 0)
                     */
                    
                    let jsonData = try? JSONSerialization.data(withJSONObject: arrCat, options: [])
                    let jsonString = String(data: jsonData!, encoding: .utf8)
                    
                    let data = jsonString!.data(using: String.Encoding.utf8)!
                    
                    let userResponse = try JSONDecoder().decode([Category_info].self, from: data)
                    completion(userResponse)
                    
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func blackHistoryInfo(completion:@escaping (blackModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.todayblackhistory)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(blackModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func bannerApi( completion:@escaping (Any)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.homepageboxmessage)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                
                completion(response.value!)
            //let userResponse = try JSONDecoder().decode(bannerModel.self, from: response.data!)
            //completion(userResponse)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func top10ArticleListApi(userId:String ,completion:@escaping (top10Model)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.articles_top10)?userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(top10Model.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func belowArticleListApi(userId:String, pageNo:String ,completion:@escaping (Any)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.below_top10)?user_id=\(userId)&page=\(pageNo)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    static func belowArticlebyCatId(userId:String, pageNo:String, cat_id:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.articlelistbycategory)?cat_id=\(cat_id)&userid=\(userId)&page=\(pageNo)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                // do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //}catch let err{
            //  print(err.localizedDescription)
            //}
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func belowArticlebyPublisherId(userId:String, pageNo:String, publisher_id:String ,completion:@escaping (Any)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.articlelistbypublisher)?publisher_id=\(publisher_id)&userid=\(userId)&page=\(pageNo)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            // }catch let err{
            //  print(err.localizedDescription)
            //}
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func searchArticlebyKeyword(userId:String, pageNo:String, keyword:String ,completion:@escaping (Any)->Void){
        
        let keywrdSpace = keyword.replacingOccurrences(of: " ", with: "%20")
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.searcharticles)?keyword=\(keywrdSpace)&page=\(pageNo)&userid=\(userId)"
        
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //  do{
                // let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            // }catch let err{
            //     print(err.localizedDescription)
            // }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func getPrivacy(strTyp: String, completion:@escaping (privacyModel)->Void){
        var url = ""
        if(strTyp == "privacy"){
            url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.privacy_policy)"
        }
        else if(strTyp == "terms"){
            url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.terms_of_service)"
        }
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(privacyModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func sendFeedBack(userId:String, msg:String, completion:@escaping (commonResponseModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_feedback)?userid=\(userId)&feedback_message=\(msg)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func saveLocationWithZipcode(userId:String, zipCode:String, completion:@escaping (locationModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_location)?userid=\(userId)&zipcode=\(zipCode)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(locationModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func saveLocationWithoutZip(userId:String, action:String, completion:@escaping (locationModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_location)?userid=\(userId)&action=\(action)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(locationModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func saveNotification(userId:String, notification:String, completion:@escaping (notificationModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_notification)?userid=\(userId)&notification=\(notification)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(notificationModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func uploadImgToServer(userId:String, imgName:String, keyName: String, imgData: Data, completion:@escaping (Any)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_tumbnail)?userid=\(userId)"
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(imgData, withName: keyName, fileName: imgName, mimeType: "image/jpeg")
            
        }, to: url,method:HTTPMethod.post,
        headers:nil,
        encodingCompletion: { encodingResult in
            DispatchQueue.main.async {
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        completion(response.value!)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        })
    }
    
    static func getFolderListByUSer(userId:String, completion:@escaping ([FolderList])->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_folderlist)?userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    
                    let rr = response.result.value as! NSDictionary
                    let arrFolder = rr["folderList"] as! NSArray
                    let arrNewFolderList : NSMutableArray = []
                    
                    for i in 0..<arrFolder.count{
                        let dictA = arrFolder[i] as! NSDictionary
                        let myMutableDict: NSMutableDictionary = NSMutableDictionary(dictionary: dictA)
                        myMutableDict.setValue("false", forKey: "is_select")
                        arrNewFolderList.add(myMutableDict)
                    }
                    
                    let jsonData = try? JSONSerialization.data(withJSONObject: arrNewFolderList, options: [])
                    let jsonString = String(data: jsonData!, encoding: .utf8)
                    let data = jsonString!.data(using: String.Encoding.utf8)!
                    
                    let userResponse = try JSONDecoder().decode([FolderList].self, from: data)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func getAllFolderList(userId:String, pageNo:String,  completion:@escaping (allFolderListModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_allfolderlist)?userid=\(userId)&page=\(pageNo)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(allFolderListModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func changeFolderName(userId:String, folderId:String, folderName:String,  completion:@escaping (commonResponseModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_folder_operation)?userid=\(userId)&folderId=\(folderId)&folderName=\(folderName)&action=update"
        
        
        let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        Alamofire.request(urlString!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func deleteFolder(userId:String, folderId:String,  completion:@escaping (commonResponseModel)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_folder_operation)?userid=\(userId)&folderId=\(folderId)&action=delete"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func artticleListByFolder(userId:String, pageNo:String, folderId:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_articlelistbyfolder)?userid=\(userId)&folderId=\(folderId)&page=\(pageNo)"
        
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                // do{
                //let userResponse = try JSONDecoder().decode(listByFolder.self, from: response.data!)
                completion(response.value!)
            //  }catch let err{
            //   print(err.localizedDescription)
            // }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    static func deleteArticleFromFolder(userId:String, folderId:String, articleId:String, completion:@escaping (commonResponseModel?, Error?)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_articledelete_from_folder)?userid=\(userId)&folderId=\(folderId)&articleId=\(articleId)"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse, nil)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
    static func createFolder(userId:String, folderName:String, folderDesc:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_createfolder)?userid=\(userId)&folder_name=\(folderName)&folder_desc=\(folderDesc)"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                completion(response.result.value!)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func addArticletoFolder(userId:String, folderId:String, articleId:String,  completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Folder.user_additemtofolder)?userid=\(userId)&folderId=\(folderId)&articleId=\(articleId)"
        
        let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        Alamofire.request(urlString!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    
                    let userResponse = try JSONSerialization.jsonObject(with: response.data!, options: .mutableLeaves)
                    
                    // let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func countClickViewsonArticle(userId:String, node_id:String,  completion:@escaping (viewCountsModel)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.articleviews)?userid=\(userId)&node_id=\(node_id)"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(viewCountsModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func topTenList(userId:String,  completion:@escaping (FollowingModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.user_cat_channel_list)?userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(FollowingModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func addFollowingItems(userId:String, cat_id:String, objectTyp:String,  completion:@escaping (addFollowingModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.followingitems)?uid=\(userId)&object_id=\(cat_id)&object_type=\(objectTyp)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(addFollowingModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func selectedFollowing(userId:String,  completion:@escaping (selctFollowingModel)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.user_following)?user_id=\(userId)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(selctFollowingModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func catfollowingList(userId:String, page:String, completion:@escaping (allCategoryFollowingModel)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.categorylistFollowing)?userid=\(userId)&page=\(page)"
        Alamofire.request(url).responseJSON { response in
            
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(allCategoryFollowingModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func channelList(page:String, completion:@escaping (allChannelList)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.channellist)?page=\(page)"
        Alamofire.request(url).responseJSON { response in
            
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(allChannelList.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func SearchchannelList(page:String,keyword:String, userId:String, completion:@escaping (allChannelList)->Void){
        
        let keywrdSpace = keyword.replacingOccurrences(of: " ", with: "%20")
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Following.channellist_search)?page=\(page)&keyword=\(keywrdSpace)&userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(allChannelList.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func getUerDetails(userId:String, completion:@escaping (Any)->Void){
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_details)?userid=\(userId)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                completion(response.value!)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    static func likeonArticle(userId:String, node_id:String, likes:String,  completion:@escaping (commonResponseModel)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.likearticles)?userid=\(userId)&nodeid=\(node_id)&likes=\(likes)"
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result{
            case .success( _):
                do{
                    let userResponse = try JSONDecoder().decode(commonResponseModel.self, from: response.data!)
                    completion(userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    static func sendRefeshToken(device_id:String, refreshTokens:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.usersrefreshtokens)?device_id=\(device_id)&refreshTokens=\(refreshTokens)&appname=IOS"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func getDetailArticleFromLink(aliaspath:String, userid:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Category.article_detail_alias)?aliaspath=\(aliaspath)&userid=\(userid)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    static func forgetPAssApi(userMail:String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.forgot_password)?action=request-otp&userMail=\(userMail)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func verifyOtpApi(userMail:String, userOtp: String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.forgot_password)?action=verify-otp&userMail=\(userMail)&userOtp=\(userOtp)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func resetPassApi(userMail:String, resetpassword: String ,completion:@escaping (Any)->Void){
        
        let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.User.forgot_password)?action=forgot-password&userMail=\(userMail)&resetpassword=\(resetpassword)"
        Alamofire.request(url).responseJSON { response in
            switch response.result{
            case .success( _):
                //do{
                //let userResponse = try JSONDecoder().decode(belowModel.self, from: response.data!)
                completion(response.value!)
            //                }catch let err{
            //                    print(err.localizedDescription)
            //                }
            case .failure(let error):
                print(error)
            }
        }
    }
}




